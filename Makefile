SRCS = src/ak_main.c \
       src/ak_lex.c \
       src/ak_code.c \
       src/ak_vm.c \
       src/ak_object.c \
       src/ak_string.c \
       src/ak_number.c \
       src/ak_list.c \
       src/ak_table.c \
       src/ak_time.c \
       src/ak_gc.c

DEPS = src/ak_debug.h \
       src/ak_alloc.h \
       src/ak_lex.h \
       src/ak_code.h \
       src/ak_vm.h \
       src/ak_object.h \
       src/ak_string.h \
       src/ak_number.h \
       src/ak_list.h \
       src/ak_table.h \
       src/ak_time.h \
       src/ak_gc.h


INCS = -I src/

CFLAGS = $(INCS) -O2 -g -D AK_ASSERT
CFLAGS_D = $(INCS) -O0 -g -D AK_DEBUG=1 -D AK_ASSERT
CFLAGS_G = $(INCS) -O2 -g -D AK_DEBUG_GPERF=1 -lprofiler

CC_S = clang
CFLAGS_S = $(INCS) -O0 -g -fsanitize=address -fno-omit-frame-pointer


all:ak
ak:$(SRCS) $(DEPS)
	$(CC) $(SRCS) -o $@ $(CFLAGS) #-D AK_GC_STAT
akd:$(SRCS) $(DEPS)
	$(CC) $(SRCS) -o $@ $(CFLAGS_D)
aks:$(SRCS) $(DEPS)
	$(CC_S) $(SRCS) -o $@ $(CFLAGS_S)

ak-gperf:$(SRCS) $(DEPS)
	$(CC) $(SRCS) -o $@ $(CFLAGS_G) -L /usr/lib/ -L /usr/local/lib/
ak-gperfn:$(SRCS) $(DEPS)
	$(CC) $(SRCS) -o $@ $(CFLAGS_G) -L /usr/lib/ -L /usr/local/lib/

.PHONY: clean lldb gdb tags
clean:
	rm -rf ak akd aks ak-gperf *.dSYM tags *.log *.pdf *.profile cscope.out

tags:$(SRCS) $(DEPS)
	ctags -R src
	cscope -bR

lldb:ak
	lldb -- ./ak test.ak
gdb:ak
	gdb --args ./ak test.ak
