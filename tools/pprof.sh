# use google perf tools to optimize performance

AK=./ak-gperf
AKFILE=test/eight_queens_puzzle.ak
if [ "$1" != "" ]; then
    AKFILE=$1
fi


echo "+ make $AK"
make $AK

echo
echo "+ run $AKFILE"
time $AK $AKFILE

echo
echo "+ pprof: analysis profile"

pprof --text $AK ./ak.profile > ./ak-gperf.txt
# ps2pdf command not found:
#  $ sudo yum install ghostscript
pprof --pdf $AK ./ak.profile > ./ak-gperf.pdf

echo
echo "+ generated report: ak-gperf.txt"
echo "+ generated report: ak-gperf.pdf"
