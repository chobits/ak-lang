" Vim syntax file
" Language:   ak
" Maintainer: Xiaochen Wang <wangxiaochen0@gmail.com>
" Last Change: 2014 Dec 9

" Quit when a (custom) syntax file was already loaded
if exists("b:current_syntax")
  finish
endif

syn keyword     ak_operator     and or
syn keyword     ak_conditional  if else
syn keyword     ak_repeat       while
syn keyword     ak_builtinfunc  print len assert

syn match       ak_function     "func\s\w" contained
syn keyword     ak_statement    break continue
syn keyword     ak_statement    return
syn keyword     ak_statement    func nextgroup=ak_function skipwhite


" comment & todo, no spell checking
syn keyword     ak_todo         contained TODO FIXME XXX NOTE
syn match       ak_comment      "#.*$" contains=ak_todo,@Spell

" String
syn match       ak_string       "\"[^"]*\""

" Number
syn match       ak_number       "\d\+"
syn match       ak_number       "[-+]\d\+"
syn match       ak_number       "\d\+\.\d*"
syn match       ak_number       "[-+]\d\+\.\d*"

hi def link ak_statement        Statement
hi def link ak_builtinfunc      Function
hi def link ak_comment          Comment
hi def link ak_number           Constant
hi def link ak_string           String
hi def link ak_conditional      Conditional
hi def link ak_repeat           Repeat
hi def link ak_function         Function
hi def link ak_operator         Operator

let b:current_syntax = "ak"

" vim:set sw=2 sts=2 ts=8 noet:
