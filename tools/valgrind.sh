if [ $1 ]; then
  valgrind -q --log-file=memleak.log --tool=memcheck --leak-check=full ./ak $1
  exit 0
fi

CHECK_AKS=$(ls test | grep -v eight | grep "ak$")
ERROR_AKS=""

for AK in $CHECK_AKS
do
  echo "+ test/$AK"
  valgrind -q --log-file=$AK.memleak.log --tool=memcheck --leak-check=full ./ak test/$AK
  #--read-var-info=yes
  if [ -s $AK.memleak.log ]; then
    ERROR_AKS="$ERROR_AKS $AK"
  else
    rm -f $AK.memleak.log
  fi
  echo ""
done

if [ "$ERROR_AKS" ]; then
  echo "+ Error occurs while running ak sources:"
  echo " $ERROR_AKS"
else
  echo "+ No error found with valgrind memcheck tool"
fi
