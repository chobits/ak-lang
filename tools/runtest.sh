AK=./aks
if [ "$1" != "" ]; then
    AK=$1
fi

CHECK_AKS=$(ls test | grep "ak$")
ERROR_AKS=""

echo "+ make $AK"
make $AK

for AKFILE in $CHECK_AKS
do
  echo
  echo "=================== $AKFILE"
  time $AK test/$AKFILE
  if [ "$?" != "0" ]; then
      echo "ERROR: test case failed."
      exit 1
  fi
done
