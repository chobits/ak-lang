#include <ak_lex.h>
#include <ak_code.h>
#include <ak_alloc.h>
#include <ak_object.h>
#include <ak_string.h>
#include <ak_number.h>
#include <ak_debug.h>
#include <ak_vm.h>
#include <ak_table.h>
#include <ak_debug.h>
#include <ak_gc.h>

#include <err.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>

/* some macro to optimize opcodes */
#define AK_CODE_DROP_IF_LAST_JMP 1

static void ak_code_stmt(struct ak_lexer *lex, struct ak_code *code);
static void ak_code_expr_stmt(struct ak_lexer *lex, struct ak_code *code);

static const char *ak_opc_str[] = {
#define AK_OPC_STR(name) ""#name,
AK_OPC_DEF(AK_OPC_STR)
#undef AK_OPC_STR
};

#define ak_get_tokenC(lex)                                  \
    do {                                                    \
        ak_get_token(lex);                                  \
        if (lex->tk.type == AK_TK_ERR) {                    \
            ak_code_syntax_error(lex, code, "lex error");   \
            return ;                                        \
        }                                                   \
    } while (0)

/* AK API: */
char *ak_opc2str(uint32_t opc)
{
    ak_assert(opc < AK_OPC__MAX);
    return (char *) ak_opc_str[opc];
}

static void ak_code_clear(struct ak_code *code)
{
#if 0
    ak_table_free_string_keys(code->upvalue);
#endif
    ak_table_free(code->upvalue);
#if 0
    ak_table_free_string_keys(code->variable);
#endif
    ak_table_free(code->variable);
}

static void ak_code_emit(struct ak_code *code, uint32_t opc, uint32_t opr)
{
    if (code->opnum >= AK_MAX_OPCODE) {
        errx(-1, "too many opcodes");
    }
    ak_debug("+ %d: %-8s %d\n", code->opnum, ak_opc2str(opc), opr);
    code->opcode[code->opnum++] = AK_OPCODE_EMIT(opc, opr);
}

static void ak_code_fix(struct ak_code *code, int opidx, uint32_t opr)
{
    if (opidx >= code->opnum) {
        errx(-1, "try to fix opcode out of range");
    }
    ak_debug("+ <fix> %d: %-8s %d\n", opidx, ak_opc2str(AK_OPCODE_OPC(code->opcode[opidx])), opr);
    code->opcode[opidx] = AK_OPCODE_EMIT(AK_OPCODE_OPC(code->opcode[opidx]), opr);
}

/* fix operand of JMP opcode to be @fixedidx from @eidx to @sidx */
int ak_code_fix_jmp_list(struct ak_code *code, int sidx, int eidx, int fixedidx)
{
    int delta;
    while (sidx < eidx) {
        if (AK_OPCODE_OPC(code->opcode[eidx]) != AK_OPC_JMP) {
            errx(-1, "The list of jmp opcodes corrupts.");
        }
        delta = AK_OPCODE_OPR(code->opcode[eidx]);
        ak_code_fix(code, eidx, fixedidx - eidx);
        eidx -= delta;
    }
    return eidx;
}

static int ak_code_add_constant_object(struct ak_code *code, void *obj)
{
    int i;
    for (i = 0; i < code->constnum; i++) {
        if (akc_equal(code->constant[i], obj)) {
            return i;
        }
    }
    if (code->constnum >= AK_MAX_CONSTANT) {
        errx(-1, "too many constant");
    }
    code->constant[code->constnum++] = obj;
    return code->constnum - 1;
}

static int ak_code_add_constant(struct ak_lexer *lex, struct ak_code *code)
{
    int len, n;
    char *data;
    void *obj;

    len = (lex->tk.buf.pos - lex->tk.buf.start);
    data = (char *) lex->tk.buf.start;
    ak_debug("[C] add constant %s `%.*s'\n", ak_tk2str(lex->tk.type), len, data);
    if (lex->tk.type == AK_TK_NUM) {
        obj = ak_vm_number_strn(lex->vm, data, len);
        if (obj == NULL) {
            errx(-1, "lex: invalid NUM constant `%.*s'", len, data);
        }
    } else if (lex->tk.type == AK_TK_VAR || lex->tk.type == AK_TK_STR) {
        obj = ak_vm_string(lex->vm, data, len);
        if (obj == NULL) {
            errx(-1, "lex: invalid STR constant `%.*s'", len, data);
        }
    } else {
        errx(-1, "lex: add unknown type object to constant");
    }
    n = ak_code_add_constant_object(code, obj);
    return n;
}

static int ak_code_add_variable_object(struct ak_lexer *lex, struct ak_code *code, void *obj)
{
    int index;
    void *key;
    index = (intptr_t) ak_table_get_kv(code->variable, (struct ak_object *) obj, (struct ak_object **) &key);
    if (index == 0) {
        if (code->varnum >= AK_MAX_VAR) {
            errx(-1, "too many variable");
        }
        index = ++code->varnum;  /* index starts from 1 */
        ak_table_set(code->variable, (struct ak_object *) obj, (struct ak_object *) (intptr_t) index);
#if 0
    } else if (key != obj) {
        ak_free(obj);   /* free duplicated object */
#endif
    }
    return index;
}

static int ak_code_add_variable(struct ak_lexer *lex, struct ak_code *code)
{
    int len;
    char *data;
    void *obj;
    len = (lex->tk.buf.pos - lex->tk.buf.start);
    data = (char *) lex->tk.buf.start;
    ak_debug("[V] add variable %s `%.*s'\n", ak_tk2str(lex->tk.type), len, data);
    if (lex->tk.type != AK_TK_VAR) {
        errx(-1, "lex: add unsupport type object to variable");
    }
    obj = ak_vm_string(lex->vm, data, len); /* non gc */
    return ak_code_add_variable_object(lex, code, obj);
}

static void ak_code_syntax_error(struct ak_lexer *lex, struct ak_code *code, char *errmsg)
{
    code->syntax_error = 1;
    code->syntax_errmsg = errmsg;
}

/* BNF */
static void ak_code_list_stmt(struct ak_lexer *lex, struct ak_code *code)
{
    uint32_t operand = 0;

    ak_get_tokenC(lex);
    if (lex->tk.type == AK_TK_RSP) {
        ak_code_emit(code, AK_OPC_LIST, operand);
        return;
    }
    ak_unget_token(lex);

    /* [ */
    while (1) {
        /* expr-stmt */
        ak_code_expr_stmt(lex, code);
        if (code->syntax_error) {
            return;
        }
        operand++;
        /* , */
        ak_get_tokenC(lex);
        if (lex->tk.type != AK_TK_COMMA) {
            break;
        }
    }
    /* ] */
    if (lex->tk.type != AK_TK_RSP) {
        ak_code_syntax_error(lex, code, "list-stmt: expect `]'");
        return;
    }

    ak_code_emit(code, AK_OPC_LIST, operand);
}

/* BNF */
static void ak_code_table_stmt(struct ak_lexer *lex, struct ak_code *code)
{
    uint32_t operand = 0;

    ak_get_tokenC(lex);
    if (lex->tk.type == AK_TK_RAP) {
        ak_code_emit(code, AK_OPC_TABLE, operand);
        return;
    }
    ak_unget_token(lex);

    /* { */
    while (1) {
        /* key: expr-stmt */
        ak_code_expr_stmt(lex, code);
        if (code->syntax_error) {
            return;
        }
        /* : */
        ak_get_tokenC(lex);
        if (lex->tk.type != AK_TK_COLON) {
            ak_code_syntax_error(lex, code, "table-stmt: expect `:'");
            return;
        }
        /* value: expr-stmt */
        ak_code_expr_stmt(lex, code);
        if (code->syntax_error) {
            return;
        }
        operand += 2;
        /* , */
        ak_get_tokenC(lex);
        if (lex->tk.type != AK_TK_COMMA) {
            break;
        }
    }
    /* } */
    if (lex->tk.type != AK_TK_RAP) {
        ak_code_syntax_error(lex, code, "table-stmt: except `}'");
        return;
    }

    ak_code_emit(code, AK_OPC_TABLE, operand);
}

static int ak_code_lookup_upvalue_object(struct ak_lexer *lex, struct ak_code *code, struct ak_object *obj)
{
    int index, level;
    struct ak_code *c;
    ak_debug("[V] lookup variable `%s'\n", ak_str2data(obj));
    /* again */
    index = (intptr_t) ak_table_get(code->upvalue, obj);
    if (index != 0) {
#if 0
        ak_free(obj);
#endif
        return index;
    }
    /* first */
    for (level = 1, c = code->back; c != NULL; level++, c = c->back) {
        index = (intptr_t) ak_table_get(c->variable, obj);
        if (index != 0) {
            break;
        }
    }
    if (index == 0) {   /* not found in upvalues */
        return 0;
    }
    if (code->upvalnum >= AK_MAX_UPVALUE) {
        errx(-1, "too many upvalues");
    }
    code->upvalidx[code->upvalnum++] = (index | (level << 8));
    ak_table_set(code->upvalue, obj, (struct ak_object *) (intptr_t) code->upvalnum);
    return code->upvalnum;
}

static void ak_code_var(struct ak_lexer *lex, struct ak_code *code)
{
    struct ak_object *obj;
    size_t len;
    char *data;
    int index;

    len = (lex->tk.buf.pos - lex->tk.buf.start);
    data = (char *) lex->tk.buf.start;
    obj = (struct ak_object *) ak_vm_string(lex->vm, data, len);    /* non gc */
    if (obj == NULL) {
        errx(-1, "cannot alloc string for VAR");
    }

    /* check whether VAR is assign-stmt left value */
    ak_get_tokenC(lex);
    if (lex->tk.type == AK_TK_ASSIGN) {
        code->varsaved = obj;
        ak_unget_token(lex);
        return;
    }
    ak_unget_token(lex);

    /* local variable */
    index = (intptr_t) ak_table_get(code->variable, obj);
    if (index != 0) {
        ak_code_emit(code, AK_OPC_GETV, index);
        return;
    }
    /* upvalue */
    index = ak_code_lookup_upvalue_object(lex, code, obj);  /* obj has been freed */
    if (index != 0) {
        ak_code_emit(code, AK_OPC_GETUP, index - 1);
#if 0
        ak_free(obj);
#endif
        return;
    }
    /* predefined value */
    ak_debug("[C] add constant %s\n", ak_str2data(obj));
    index = ak_code_add_constant_object(code, obj);
#if 0   /* gc */
    if (obj != code->constant[index]) {
        ak_free(obj);   /* free duplicated object */
    }
#endif
    ak_code_emit(code, AK_OPC_GETC, index);
    ak_code_emit(code, AK_OPC_GET, 1);
}

/* BNF */
static void ak_code_block_stmt(struct ak_lexer *lex, struct ak_code *code)
{
    ak_get_tokenC(lex);
    /* NUM */
    if (lex->tk.type == AK_TK_NUM) {
        ak_code_emit(code, AK_OPC_GETC, ak_code_add_constant(lex, code));
        return;
    }
    /* VAR */
    if (lex->tk.type == AK_TK_VAR) {
        ak_code_var(lex, code);
        return;
    }
    /* STR */
    if (lex->tk.type == AK_TK_STR) {
        ak_code_emit(code, AK_OPC_GETC, ak_code_add_constant(lex, code));
        return;
    }
    /* list-stmt */
    if (lex->tk.type == AK_TK_LSP) {
        ak_code_list_stmt(lex, code);
        return;
    }
    /* table-stmt */
    if (lex->tk.type == AK_TK_LAP) {
        ak_code_table_stmt(lex, code);
        return;
    }
    /* ( expr-stmt ) */
    if (lex->tk.type == AK_TK_LP) {
        ak_code_expr_stmt(lex, code);
        if (code->syntax_error) {
            return;
        }
        ak_get_tokenC(lex);
        if (lex->tk.type != AK_TK_RP) {
            ak_code_syntax_error(lex, code, "expr-stmt: expect `)' after expr-stmt");
            return;
        }
        return;
    }
    /* syntax error */
    ak_code_syntax_error(lex, code, "");
    return;
}

/* BNF */
static void ak_code_getfield_part_stmt(struct ak_lexer *lex, struct ak_code *code)
{
    while (1) {
        /* VAR */
        ak_get_tokenC(lex);
        if (lex->tk.type != AK_TK_VAR) {
            ak_code_syntax_error(lex, code, "getfield-part-stmt: expect VAR after `.'");
            return;
        }
        ak_code_emit(code, AK_OPC_GETC, ak_code_add_constant(lex, code));
        ak_code_emit(code, AK_OPC_GET, 2);

        /* . */
        ak_get_tokenC(lex);
        if (lex->tk.type != AK_TK_DOT) {
            ak_unget_token(lex);
            return;
        }
    }
}

/* BNF */
static void ak_code_index_part_stmt(struct ak_lexer *lex, struct ak_code *code)
{
    ak_code_expr_stmt(lex, code);
    if (code->syntax_error) {
        return;
    }

    ak_get_tokenC(lex);
    if (lex->tk.type != AK_TK_RSP) {
        ak_code_syntax_error(lex, code, "index-part-stmt: expect `]' after key");
        return;
    }

    ak_code_emit(code, AK_OPC_GET, 2);
}

/* BNF */
static void ak_code_call_part_stmt(struct ak_lexer *lex, struct ak_code *code)
{
    uint32_t argnum = 0;

    ak_get_tokenC(lex);
    if (lex->tk.type == AK_TK_RP) {
        ak_code_emit(code, AK_OPC_CALL, argnum);
        return;
    }
    ak_unget_token(lex);

    /* ( */
    /* arguments */
    while (1) {
        ak_code_expr_stmt(lex, code);
        if (code->syntax_error) {
            return;
        }
        argnum++;
        /* , */
        ak_get_tokenC(lex);
        if (lex->tk.type != AK_TK_COMMA) {
            break;
        }
    }
    /* ) */
    if (lex->tk.type != AK_TK_RP) {
        ak_code_syntax_error(lex, code, "call-part-stmt: expect `)' after arguments");
        return;
    }

    ak_code_emit(code, AK_OPC_CALL, argnum);
}

/* BNF */
/* ak_code_getfield_stmt() / ak_code_index_stmt() / ak_code_call_stmt() */
static void ak_code_getfield_index_call_stmt(struct ak_lexer *lex, struct ak_code *code)
{
    ak_code_block_stmt(lex, code);
    if (code->syntax_error) {
        return;
    }
    while (1) {
        ak_get_tokenC(lex);
        if (lex->tk.type == AK_TK_LP) {
            ak_code_call_part_stmt(lex, code);
        } else if (lex->tk.type == AK_TK_LSP) {
            ak_code_index_part_stmt(lex, code);
        } else if (lex->tk.type == AK_TK_DOT) {
            ak_code_getfield_part_stmt(lex, code);
        } else {
            ak_unget_token(lex);
            return;
        }
    }
}

/* BNF */
static void ak_code_mul_stmt(struct ak_lexer *lex, struct ak_code *code)
{
    uint32_t opcode;

    ak_code_getfield_index_call_stmt(lex, code);
    if (code->syntax_error) {
        return;
    }

    while (1) {
        ak_get_tokenC(lex);
        if (lex->tk.type == AK_TK_MUL) {
            opcode = AK_OPC_MUL;
        } else if (lex->tk.type == AK_TK_DIV) {
            opcode = AK_OPC_DIV;
        } else if (lex->tk.type == AK_TK_MOD) {
            opcode = AK_OPC_MOD;
        } else {
            ak_unget_token(lex);
            return;
        }

        ak_code_getfield_index_call_stmt(lex, code);
        if (code->syntax_error) {
            return;
        }

        ak_code_emit(code, opcode, 2);
    }
}

/* BNF */
static void ak_code_add_stmt(struct ak_lexer *lex, struct ak_code *code)
{
    uint32_t opcode;

    ak_code_mul_stmt(lex, code);
    if (code->syntax_error) {
        return;
    }

    while (1) {
        ak_get_tokenC(lex);
        if (lex->tk.type == AK_TK_ADD) {
            opcode = AK_OPC_ADD;
        } else if (lex->tk.type == AK_TK_SUB) {
            opcode = AK_OPC_SUB;
        } else {
            ak_unget_token(lex);
            return;
        }

        ak_code_mul_stmt(lex, code);
        if (code->syntax_error) {
            return;
        }

        ak_code_emit(code, opcode, 2);
    }
}

/* BNF */
static void ak_code_bit_stmt(struct ak_lexer *lex, struct ak_code *code)
{
    uint32_t opcode;

    ak_code_add_stmt(lex, code);
    if (code->syntax_error) {
        return;
    }

    while (1) {
        ak_get_tokenC(lex);
        if (lex->tk.type == AK_TK_BAND) {
            opcode = AK_OPC_BAND;
        } else if (lex->tk.type == AK_TK_BXOR) {
            opcode = AK_OPC_BXOR;
        } else if (lex->tk.type == AK_TK_BOR) {
            opcode = AK_OPC_BOR;
        } else if (lex->tk.type == AK_TK_BSHL) {
            opcode = AK_OPC_BSHL;
        } else if (lex->tk.type == AK_TK_BSHR) {
            opcode = AK_OPC_BSHR;
        } else {
            ak_unget_token(lex);
            return;
        }

        ak_code_add_stmt(lex, code);
        if (code->syntax_error) {
            return;
        }

        ak_code_emit(code, opcode, 2);
    }
}

/* BNF */
static void ak_code_cmp_stmt(struct ak_lexer *lex, struct ak_code *code)
{
    uint32_t opcode;

    ak_code_bit_stmt(lex, code);
    if (code->syntax_error) {
        return;
    }

    while (1) {
        ak_get_tokenC(lex);
        if (lex->tk.type == AK_TK_EQ) {
            opcode = AK_OPC_EQ;
        } else if (lex->tk.type == AK_TK_NEQ) {
            opcode = AK_OPC_NEQ;
        } else if (lex->tk.type == AK_TK_GT) {
            opcode = AK_OPC_GT;
        } else if (lex->tk.type == AK_TK_GE) {
            opcode = AK_OPC_GE;
        } else if (lex->tk.type == AK_TK_LT) {
            opcode = AK_OPC_LT;
        } else if (lex->tk.type == AK_TK_LE) {
            opcode = AK_OPC_LE;
        } else {
            ak_unget_token(lex);
            return;
        }

        ak_code_bit_stmt(lex, code);
        if (code->syntax_error) {
            return;
        }

        ak_code_emit(code, opcode, 2);
    }
}

/* BNF */
static void ak_code_logic_stmt(struct ak_lexer *lex, struct ak_code *code)
{
    uint32_t opcode;

    ak_code_cmp_stmt(lex, code);
    if (code->syntax_error) {
        return;
    }

    while (1) {
        ak_get_tokenC(lex);
        if (lex->tk.type == AK_TK_AND) {
            opcode = AK_OPC_AND;
        } else if (lex->tk.type == AK_TK_OR) {
            opcode = AK_OPC_OR;
        } else {
            ak_unget_token(lex);
            return;
        }

        ak_code_cmp_stmt(lex, code);
        if (code->syntax_error) {
            return;
        }

        ak_code_emit(code, opcode, 2);
    }
}

/* BNF */
static void ak_code_expr_stmt(struct ak_lexer *lex, struct ak_code *code)
{
    ak_code_logic_stmt(lex, code);
}

/* BNF */
static void ak_code_if_stmt(struct ak_lexer *lex, struct ak_code *code)
{
    int jmpidx, jmpfidx, delta;

    jmpidx = 0;

    while (1) {
        jmpfidx = -1;
        /* if */
        if (lex->tk.type == AK_TK_IF) {
            /* ( */
            ak_get_tokenC(lex);
            if (lex->tk.type != AK_TK_LP) {
                ak_code_syntax_error(lex, code, "if-stmt: expect `(' after IF");
                return;
            }
            /* expr */
            ak_code_expr_stmt(lex, code);
            if (code->syntax_error) {
                return;
            }
            /* ) */
            ak_get_tokenC(lex);
            if (lex->tk.type != AK_TK_RP) {
                ak_code_syntax_error(lex, code, "if-stmt: expect `)'");
                return;
            }

            jmpfidx = code->opnum;   /* save to fix operand of jmpf opcode */
            ak_code_emit(code, AK_OPC_JMPF, 0);

            ak_get_tokenC(lex);
        }

        /* { */
        if (lex->tk.type != AK_TK_LAP) {
            ak_code_syntax_error(lex, code, "if-stmt: expect `{'");
            return;
        }
        /* stmt */
        ak_code_stmt(lex, code);
        if (code->syntax_error) {
            return;
        }
        /* } */
        ak_get_tokenC(lex);
        if (lex->tk.type != AK_TK_RAP) {
            ak_code_syntax_error(lex, code, "if-stmt: expect `}'");
            return;
        }
        /* emit jmp */
        ak_code_emit(code, AK_OPC_JMP, code->opnum - jmpidx);
        jmpidx = code->opnum - 1;

        /* fix jmpf */
        if (jmpfidx >= 0) {
            ak_code_fix(code, jmpfidx, code->opnum - jmpfidx);
        }

        /* else {} | else if {} */
        ak_get_tokenC(lex);
        if (lex->tk.type != AK_TK_ELSE) {
            ak_unget_token(lex);
            break;
        }

        ak_get_tokenC(lex);
    }

    if (AK_OPCODE_OPC(code->opcode[code->opnum - 1]) != AK_OPC_JMP) {
        errx(-1, "Last opcode of if-stmt is not JMP");
    }

#if AK_CODE_DROP_IF_LAST_JMP
    /* fix all jmp opcodes */
    (void) ak_code_fix_jmp_list(code, 0, jmpidx, code->opnum - 1);
    /* drop last jmp opcode */
    ak_debug("+ %d: drop last JMP opcode in if-stmt\n", code->opnum - 1);
    code->opnum--;  /* drop last jmp opcode */
    if (jmpfidx >= 0) {
        ak_code_fix(code, jmpfidx, code->opnum - jmpfidx);
    }
#else
    /* fix all jmp opcodes */
    (void) ak_code_fix_jmp_list(code, 0, jmpidx, code->opnum);
#endif
}

/* BNF */
static void ak_code_while_stmt(struct ak_lexer *lex, struct ak_code *code)
{
    int sidx, jmpfidx;

    sidx = code->whilesidx;
    code->whilesidx = code->opnum;
    code->whilelevel++;

    /* while */
    jmpfidx = -1;
    /* ( */
    ak_get_tokenC(lex);
    if (lex->tk.type == AK_TK_LP) {
        /* expr */
        ak_code_expr_stmt(lex, code);
        if (code->syntax_error) {
            return;
        }
        /* ) */
        ak_get_tokenC(lex);
        if (lex->tk.type != AK_TK_RP) {
            ak_code_syntax_error(lex, code, "while-stmt: expect `)'");
            return;
        }
        jmpfidx = code->opnum;
        ak_code_emit(code, AK_OPC_JMPF, 0);

        ak_get_tokenC(lex);
    }

    /* { */
    if (lex->tk.type != AK_TK_LAP) {
        ak_code_syntax_error(lex, code, "while-stmt: expect `{'");
        return;
    }
    /* stmt */
    ak_code_stmt(lex, code);
    if (code->syntax_error) {
        return;
    }
    /* } */
    ak_get_tokenC(lex);
    if (lex->tk.type != AK_TK_RAP) {
        ak_code_syntax_error(lex, code, "while-stmt: expect `}'");
        return;
    }

    ak_code_emit(code, AK_OPC_JMPB, code->opnum - code->whilesidx);
    if (jmpfidx >= 0) {
        ak_code_fix(code, jmpfidx, code->opnum - jmpfidx);
    }

    /* fix JMP opcodes of break-stmt */
    code->breakidx = ak_code_fix_jmp_list(code, code->whilesidx, code->breakidx, code->opnum);
    code->whilesidx = sidx;
    code->whilelevel--;
}

/* BNF */
static void ak_code_break_stmt(struct ak_lexer *lex, struct ak_code *code)
{
    /* BREAK */
    if (code->whilelevel == 0) {
        ak_code_syntax_error(lex, code, "break-stmt: 'break' outside loop");
        return;
    }
    ak_code_emit(code, AK_OPC_JMP, code->opnum - code->breakidx);
    code->breakidx = code->opnum - 1;
}

/* BNF */
static void ak_code_continue_stmt(struct ak_lexer *lex, struct ak_code *code)
{
    /* CONTINUE */
    if (code->whilelevel == 0) {
        ak_code_syntax_error(lex, code, "continue-stmt: 'continue' outside loop");
        return;
    }
    ak_code_emit(code, AK_OPC_JMPB, code->opnum - code->whilesidx);
}

/* BNF */
static void ak_code_return_stmt(struct ak_lexer *lex, struct ak_code *code)
{
    /* RETURE */
    ak_get_tokenC(lex);
    /* expr-stmt */
    /* check after-set of return-stmt */
    if (lex->tk.type != AK_TK_EOF && lex->tk.type != AK_TK_RAP) {
        ak_unget_token(lex);
        ak_code_expr_stmt(lex, code);
        ak_code_emit(code, AK_OPC_RET, 1);  /* TODO: return multi value */
        return;
    }
    /* no expr-stmt */
    ak_unget_token(lex);
    ak_code_emit(code, AK_OPC_RET, 0);
}

/* BNF */
/* assign-stmt | call-stmt */
static void ak_code_assign_call_stmt(struct ak_lexer *lex, struct ak_code *code)
{
    int varindex;
    uint32_t opc, opr;
    struct ak_object *var;

    ak_unget_token(lex);
    /* left-value */
    ak_code_getfield_index_call_stmt(lex, code);
    /* = */
    ak_get_tokenC(lex);
    if (lex->tk.type != AK_TK_ASSIGN) { /* call-stmt, not assign-stmt */
        if (AK_OPCODE_OPC(code->opcode[code->opnum - 1]) != AK_OPC_CALL) {
            ak_code_syntax_error(lex, code, "assign-stmt: expect =");
            return;
        }
        /* pop unused return value from stack */
        opr = AK_OPCODE_OPR(code->opcode[code->opnum - 1]) | AK_OPR_CALL_NORET;
        ak_code_fix(code, code->opnum - 1, opr);

        ak_unget_token(lex);
        return;
    }
    /* check left-value */
    var = code->varsaved;
    code->varsaved = NULL;
    if (var == NULL) {
        opc = AK_OPCODE_OPC(code->opcode[code->opnum - 1]);
        opr = AK_OPCODE_OPR(code->opcode[code->opnum - 1]);
        code->opnum--;      /* delete GET opcode */
        if (opc != AK_OPC_GET) {
            ak_code_syntax_error(lex, code, "assign-stmt: left-value cannot be call-stmt");
            return;
        }
        opc = AK_OPC_SET;
        opr += 1 /* right value */;
    } else {
        if (0 /* TODO */) {
            opc = AK_OPC_SETUP;
            /* opr = ak_code_add_upvalue_object(code, var); */
        } else {
            opc = AK_OPC_SETV;
            opr = ak_code_add_variable_object(lex, code, var);
        }
    }
    /* expr-stmt */
    ak_code_expr_stmt(lex, code);
    /* emit SET/SETV opcode */
    ak_code_emit(code, opc, opr);
}

/* BNF */
static void ak_code_func_stmt(struct ak_lexer *lex, struct ak_code *code)
{
    struct ak_code *fcode;
    int fvarindex, varindex;

    fcode = ak_vm_code(lex->vm);
    if (fcode == NULL) {
        /* error handling */
        return;
    }
    fcode->back = code;

    /* func */

    /* VAR */
    ak_get_tokenC(lex);
    if (lex->tk.type != AK_TK_VAR) {
        ak_code_syntax_error(lex, code, "func-stmt: expect VAR for function name");
        return;
    }
    fvarindex = ak_code_add_variable(lex, code);

    /* ( */
    ak_get_tokenC(lex);
    if (lex->tk.type != AK_TK_LP) {
        ak_code_syntax_error(lex, code, "func-stmt: expect `(' after function name");
        return;
    }

    ak_get_tokenC(lex);
    if (lex->tk.type == AK_TK_RP) { /* () no args */
        goto emit_func;
    }
    ak_unget_token(lex);

    /* func-args */
    while (1) {
        /* VAR */
        ak_get_tokenC(lex);
        if (lex->tk.type != AK_TK_VAR) {
            ak_code_syntax_error(lex, code, "func-args-stmt: expect VAR");
            return;
        }
        fcode->argnum++;
        varindex = ak_code_add_variable(lex, fcode);
        if (varindex != fcode->argnum) {
            ak_code_syntax_error(lex, code, "func-args-stmt: duplicated VAR");
            return;
        }
        if (fcode->argnum == AK_MAX_ARG) {
            ak_code_syntax_error(lex, code, "func-args-stmt: too many arguments");
            return;
        }
        /* , */
        ak_get_tokenC(lex);
        if (lex->tk.type != AK_TK_COMMA) {
            break;
        }
    }
    /* ) */
    if (lex->tk.type != AK_TK_RP) {
        ak_code_syntax_error(lex, code, "func-stmt: expect `)' after func-args-stmt");
        return;
    }

emit_func:

    /* { */
    ak_get_tokenC(lex);
    if (lex->tk.type != AK_TK_LAP) {
        ak_code_syntax_error(lex, code, "func-stmt: expect `{'");
        return;
    }
    /* stmt */
    ak_code_stmt(lex, fcode);
    if (fcode->syntax_error) {
        code->syntax_error = 1;
        return;
    }
    ak_code_clear(fcode);
#if (AK_DEBUG)
    ak_code_debug(lex, fcode);
#endif
    /* } */
    ak_get_tokenC(lex);
    if (lex->tk.type != AK_TK_RAP) {
        ak_code_syntax_error(lex, code, "func-stmt: expect `}'");
        return;
    }
    /* add RET opcode in the end of function */
    if (AK_OPCODE_OPC(fcode->opcode[fcode->opnum - 1]) != AK_OPC_RET) {
        ak_code_emit(fcode, AK_OPC_RET, 0);
    }

    ak_debug("[C] add constant CODE\n");
    ak_code_emit(code, AK_OPC_GETC, ak_code_add_constant_object(code, fcode));
    ak_code_emit(code, AK_OPC_CLOSURE, 1);
    ak_code_emit(code, AK_OPC_SETV, fvarindex);
}

/* BNF */
static void ak_code_stmt_block(struct ak_lexer *lex, struct ak_code *code)
{
    switch (lex->tk.type) {
        case AK_TK_IF:
            ak_code_if_stmt(lex, code);
            break;
        case AK_TK_WHILE:
            ak_code_while_stmt(lex, code);
            break;
        case AK_TK_BREAK:
            ak_code_break_stmt(lex, code);
            break;
        case AK_TK_CONTINUE:
            ak_code_continue_stmt(lex, code);
            break;
        case AK_TK_RETURN:
            ak_code_return_stmt(lex, code);
            break;
        case AK_TK_FUNC:
            ak_code_func_stmt(lex, code);
            break;
            /* call-stmt/assign-stmt first set */
        case AK_TK_NUM:
        case AK_TK_VAR:
        case AK_TK_STR:
        case AK_TK_LSP:
        case AK_TK_LAP:
        case AK_TK_LP:
            ak_code_assign_call_stmt(lex, code);
            break;
        default:
            ak_code_syntax_error(lex, code, "stmt-block: invalid first token");
            break;
    }
}

/* BNF */
static void ak_code_stmt(struct ak_lexer *lex, struct ak_code *code)
{
    while (1) {
        ak_get_tokenC(lex);
        /* TODO: check stmt follow set, if not follow set, break */
        /* check stmt end set */
        if (lex->tk.type == AK_TK_EOF || lex->tk.type == AK_TK_RAP) {
            if (lex->tk.type == AK_TK_RAP) {
                ak_unget_token(lex);
            }
            return;
        }
        ak_code_stmt_block(lex, code);
        if (code->syntax_error) {
            return;
        }
    }
}

/* AK API: */
struct ak_code *ak_vm_code(struct ak_vm *vm)
{
    struct ak_code *code;

    ak_vm_gc_check(vm);

    code = (struct ak_code *) ak_gc_alloc_object(vm->gc, sizeof(struct ak_code));

    if (code != NULL) {
        code->type = AK_OBJ_CODE;
        code->variable = ak_table();
        if (code->variable == NULL) {
            ak_free(code);
            return NULL;
        }
        code->upvalue = ak_table();
        if (code->upvalue == NULL) {
            ak_table_free(code->variable);
            ak_free(code);
            return NULL;
        }
    }
    return code;
}

/* AK API: */
void ak_code_free(struct ak_code *code)
{
#if (AK_DEBUG)
    memset(code, 0, sizeof(struct ak_code));
#endif
    ak_free(code);
}

/* AK API: */
struct ak_code *ak_code_lex(struct ak_lexer *lex)
{
    struct ak_code *code = ak_vm_code(lex->vm);
    if (code != NULL) {
        ak_code_stmt(lex, code);
        if (AK_OPCODE_OPC(code->opcode[code->opnum - 1]) != AK_OPC_RET) {
            /* always emit final RET opcode */
            ak_code_emit(code, AK_OPC_RET, 0);
        }
        ak_code_clear(code);
    }
    return code;
}

/* AK API: */
void ak_code_debug_opcode(struct ak_code *code, int pc, char *prefix)
{
    uint32_t opc, opr;
    struct ak_object *obj;

    opc = AK_OPCODE_OPC(code->opcode[pc]);
    opr = AK_OPCODE_OPR(code->opcode[pc]);

    printf("%s %-4d %8s(%d) %d", prefix, pc, ak_opc2str(opc), opc, opr);

    if (opc == AK_OPC_GETC) {
        if (opr < code->constnum) {
            obj = code->constant[opr];
            if (obj == NULL) {
                printf(" -> NULL object\n");
            } else if (obj->type == AK_OBJ_NUM) {
                printf(" -> %f\n", ((struct ak_number *) obj)->d);
            } else if (obj->type == AK_OBJ_STR) {
                printf(" -> \"%.*s\"\n", (int)((struct ak_string *) obj)->len, ak_str2data(obj));
            } else if (obj->type == AK_OBJ_CODE) {
                printf(" -> CODE\n");
            } else{
                printf(" -> unknown object\n");
            }
        } else {
            printf(" -> invalid constant index\n");
        }
    } else {
        printf("\n");
    }
}

/* AK API: */
void ak_code_debug(struct ak_lexer *lex, struct ak_code *code)
{
    int i;
    uint32_t opc, opr;
    struct ak_token *tk;
    printf("#--- AK CODE DEBUG ---\n");
    if (code->syntax_error) {
        tk = &lex->tk;
        printf("# syntax error \"%s\", at line %d, token <%s>",
               code->syntax_errmsg, lex->lineno, ak_tk2str(tk->type));
        if (tk->buf.pos != tk->buf.start) {
            printf(" `%.*s'\n", (int)(tk->buf.pos - tk->buf.start), tk->buf.start);
        } else {
            printf("\n");
        }

    }
    for (i = 0; i < code->opnum; i++) {
        ak_code_debug_opcode(code, i, "#");
    }
}

/* AK API: */
struct ak_cfunc *ak_vm_cfunc(struct ak_vm *vm, ak_cfunc_t f)
{
    struct ak_cfunc *cf;

    ak_vm_gc_check(vm);

    cf = (struct ak_cfunc *) ak_gc_alloc_object(vm->gc, sizeof(struct ak_cfunc));

    if (cf != NULL) {
        cf->type = AK_OBJ_CFUNC;
        cf->f = f;
    }

    return cf;
}
