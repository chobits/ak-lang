#ifndef AK_GC_H
#define AK_GC_H

#include <stddef.h>
#include <sys/types.h>
#include <ak_object.h>


#define AK_GC_NONE              0x00
#define AK_GC_WHITE             0x01
#define AK_GC_BLACK             0x02
#define AK_GC_GRAY              0x03

#define AK_GC_INCREMENTAL_SIZE  1024
#define AK_GC_STEP_SWEEP_COST  128

#define AK_GC_INIT_THRESHOLD    (108 * (AK_GC_INCREMENTAL_SIZE))

#define ak_vm_gc_check(vm)                              \
    {                                                   \
        if ((vm)->gc->total >= (vm)->gc->threshold)     \
            ak_vm_gc(vm);                               \
    }

struct ak_vm;

#ifdef AK_GC_STAT
struct ak_gc_head {
    int     graynum;
    int     whitenum;
    int     blacknum;
    int     state;                 /* gc state */
    size_t  threshold;
    size_t  total;              /* total count of allocated memory */
};

struct ak_gc_stat {
    uint64_t count;
    uint64_t t;
    uint64_t maxt;

    struct ak_gc_head maxgc;
    struct ak_gc_head maxngc;
};
#endif

enum {
    AK_GC_STOP,
    AK_GC_START,
    AK_GC_MARK,
    AK_GC_SWEEP,
};

struct ak_gc {
    int     graynum;
    int     whitenum;
    int     blacknum;
    int     state;                 /* gc state */
    size_t  threshold;
    size_t  total;              /* total count of allocated memory */

    struct ak_object *allgco;       /* include all objects */
    struct ak_object **sweepgco;    /* next-sweep object for SWEEP phase */
    struct ak_object *gray;

#ifdef AK_GC_STAT
    struct ak_gc_stat stat;
#endif
};

/* AK API: */
struct ak_gc *ak_gc(void);
void ak_gc_free(struct ak_gc *gc);
void ak_gc_mark_gray(struct ak_gc *gc, struct ak_object *obj);
void ak_gc_add_black(struct ak_gc *gc, struct ak_object *obj);
void ak_gc_add_white(struct ak_gc *gc, struct ak_object *obj);
void ak_gc_add_white_all(struct ak_gc *gc, struct ak_object *obj);
void ak_gc_write_barrier(struct ak_gc *gc, struct ak_object *from_obj, struct ak_object *to_obj);

void ak_vm_gc(struct ak_vm *vm);
void ak_vm_full_gc(struct ak_vm *vm);

struct ak_object *ak_gc_alloc_object(struct ak_gc *gc, size_t size);
void *ak_gc_alloc(struct ak_gc *gc, size_t size);
void *ak_gc_realloc(struct ak_gc *gc, void *p, size_t osz, size_t sz);

#endif /* AK_GC_H */
