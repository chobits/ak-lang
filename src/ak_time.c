#include <ak_time.h>

uint64_t ak_get_current_usec(void)
{
    struct timeval tv;
    (void) gettimeofday(&tv, NULL);
    return tv.tv_sec * 1000000 + tv.tv_usec;
}
