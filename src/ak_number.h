#ifndef AK_NUMBER_H
#define AK_NUMBER_H

#include <stddef.h>
#include <ak_object.h>

struct ak_vm;

struct ak_number {
    AK_OBJECT_HEAD;
    double d;
};

#define ak_num2d(n) ((struct ak_number *) (n))->d

/* AK API: */
struct ak_number *ak_vm_number(struct ak_vm *vm, double d);
struct ak_number *ak_vm_number_strn(struct ak_vm *vm, char *str, size_t len);
struct ak_number *ak_vm_number_add(struct ak_vm *vm, struct ak_number *a, struct ak_number *b);
struct ak_number *ak_vm_number_sub(struct ak_vm *vm, struct ak_number *a, struct ak_number *b);
struct ak_number *ak_vm_number_mul(struct ak_vm *vm, struct ak_number *a, struct ak_number *b);
struct ak_number *ak_vm_number_div(struct ak_vm *vm, struct ak_number *a, struct ak_number *b);
struct ak_number *ak_vm_number_mod(struct ak_vm *vm, struct ak_number *a, struct ak_number *b);

int akc_number_compare(struct ak_number *a, struct ak_number *b);

#endif  /* AK_NUMBER_H */
