#ifndef AK_OBJECT_H
#define AK_OBJECT_H

#include <stddef.h>

/* TODO: modify type name: AK_OBJ_TYPE_<type name> */
enum {
    AK_OBJ_STR,
    AK_OBJ_NUM,
    AK_OBJ_CODE,        /* AK function */
    AK_OBJ_CFUNC,       /* C  function */
    AK_OBJ_LIST,
    AK_OBJ_TABLE,
    AK_OBJ_LOCAL,
};


/*
 * @nextgco: linked into gc->allgco
 * @nextgray: linked into gc->gray
 */
#define AK_OBJECT_HEAD          \
    int type;                   \
    int color;                  \
    struct ak_object *nextgco;  \
    struct ak_object *nextgray

struct ak_object {
    AK_OBJECT_HEAD;
};

/* AK API: */
char *ak_type2str(int t);
void ak_object_debug(char *prefix, struct ak_object *obj);
void ak_object_free(struct ak_object *obj);
size_t ak_object_size(struct ak_object *obj);

#endif /* AK_OBJECT_H */
