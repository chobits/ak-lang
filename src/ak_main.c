#include <ak_alloc.h>
#include <ak_lex.h>
#include <ak_code.h>
#include <ak_object.h>
#include <ak_string.h>
#include <ak_number.h>
#include <ak_list.h>
#include <ak_table.h>
#include <ak_vm.h>
#include <ak_debug.h>
#include <ak_gc.h>

#include <err.h>
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/stat.h>

int ak_cfunc_len(struct ak_vm *vm, int argnum)
{
    struct ak_object *obj;
    struct ak_number *res;
    if (argnum != 1) {
        errx(-1, "len() has only 1 argument");
    }
    obj = ak_vm_pop(vm);    /* pop argument */
    ak_vm_pop(vm);          /* pop function */
    if (obj->type == AK_OBJ_STR) {
        res = ak_vm_number(vm, ((struct ak_string *) obj)->len);
    } else if (obj->type == AK_OBJ_LIST) {
        res = ak_vm_number(vm, ((struct ak_list *) obj)->num);
    } else if (obj->type == AK_OBJ_TABLE){
        res = ak_vm_number(vm, ((struct ak_table *) obj)->num);
    } else {
        errx(-1, "len() only uses string/list/dict as argument");
    }
    ak_vm_push(vm, res);
    return 1;
}

int ak_cfunc_print(struct ak_vm *vm, int argnum)
{
    struct ak_object *obj;
    if (argnum != 1) {
        errx(-1, "print() has only 1 argument");
    }
    obj = ak_vm_pop(vm);    /* pop argument */
    ak_vm_pop(vm);          /* pop function */
    if (obj->type == AK_OBJ_STR) {
        fputs((const char *) ak_str2data(obj), stdout);
    } else if (obj->type == AK_OBJ_NUM) {
        printf("%.14g", ak_num2d(obj)); /* format "0.14g" comes from lua_number2str() */
    } else {
        errx(-1, "print(): invalid type(%s) object", ak_type2str(obj->type));
    }
    fflush(stdout);
    return 0;
}

int ak_cfunc_assert(struct ak_vm *vm, int argnum)
{
    struct ak_object *obj;
    if (argnum != 1) {
        errx(-1, "assert() has only 1 argument");
    }
    obj = ak_vm_pop(vm);    /* pop argument */
    ak_vm_pop(vm);          /* pop function */
    if (ak_is_false(obj)) {
        errx(-1, "assert() failed (TODO: line number and function name)");
    }
    return 0;
}

int ak_cfunc_gc_collect(struct ak_vm *vm, int argnum)
{
    if (argnum != 0) {
        errx(-1, "gc_collect() has no argument");
    }
    ak_vm_pop(vm);          /* pop function */
    ak_vm_full_gc(vm);
    return 0;
}

int ak_cfunc_table_delete(struct ak_vm *vm, int argnum)
{
    struct ak_object *key;
    struct ak_table *t;

    if (argnum != 2) {
        errx(-1, "table.delete() needs 2 argument");
    }
    key = ak_vm_pop(vm);
    t = ak_vm_pop(vm);
    ak_vm_pop(vm);          /* pop function */
    if (t->type != AK_OBJ_TABLE) {
        errx(-1, "first argument of table.delete() must be table");
    }
    ak_table_delete(t, key);
    return 0;
}

int ak_cfunc_table_has(struct ak_vm *vm, int argnum)
{
    struct ak_object *key;
    struct ak_table *t;
    struct ak_number *res;

    if (argnum != 2) {
        errx(-1, "table.has() needs 2 argument");
    }
    key = ak_vm_pop(vm);
    t = ak_vm_pop(vm);
    ak_vm_pop(vm);          /* pop function */
    if (t->type != AK_OBJ_TABLE) {
        errx(-1, "first argument of table.delete() must be table");
    }
    res = (ak_table_get(t, key) == NULL) ? ak_vm_number(vm, 0) : ak_vm_number(vm, 1);
    ak_vm_push(vm, res);
    return 1;
}

int ak_cfunc_list_append(struct ak_vm *vm, int argnum)
{
    struct ak_object *obj;
    struct ak_list *l;

    if (argnum != 2) {
        errx(-1, "list.append() needs 2 argument");
    }
    obj = ak_vm_pop(vm);
    l = ak_vm_pop(vm);
    ak_vm_pop(vm);          /* pop function */
    if (l->type != AK_OBJ_LIST) {
        errx(-1, "first argument of list.append() must be list");
    }
    ak_vm_list_append(vm, l, obj);
    return 0;
}

void ak_lib_load(struct ak_vm *vm)
{
    struct ak_table *t;
    struct ak_string *s;

    /* add predefine function */
    ak_vm_set_table_cfunc(vm, vm->global, "len", (ak_cfunc_t) &ak_cfunc_len);
    ak_vm_set_table_cfunc(vm, vm->global, "print", (ak_cfunc_t) &ak_cfunc_print);
    ak_vm_set_table_cfunc(vm, vm->global, "assert", (ak_cfunc_t) &ak_cfunc_assert);
    ak_vm_set_table_cfunc(vm, vm->global, "gc_collect", (ak_cfunc_t) &ak_cfunc_gc_collect);

    /* table.delete() */
    t = ak_vm_table(vm);
    s = ak_vm_string(vm, "table", sizeof("table") - 1);
    ak_vm_set(vm, (struct ak_object *) vm->global, (struct ak_object *) s, (struct ak_object *) t);
    ak_vm_set_table_cfunc(vm, t, "has", ak_cfunc_table_has);
    ak_vm_set_table_cfunc(vm, t, "delete", ak_cfunc_table_delete);

    /* list.append() */
    t = ak_vm_table(vm);
    s = ak_vm_string(vm, "list", sizeof("list") - 1);
    ak_vm_set(vm, (struct ak_object *) vm->global, (struct ak_object *) s, (struct ak_object *) t);
    ak_vm_set_table_cfunc(vm, t, "append", ak_cfunc_list_append);
}

void ak_main(char *str, size_t len)
{
    struct ak_vm *vm;
    struct ak_local *local;

    /* vm */
    vm = ak_vm();
    if (vm == NULL) {
        errx(-1, "cannot create vm");
    }

    ak_lib_load(vm);

    /* local */
    local = ak_vm_local_strn(vm, str, len);
    if (local == NULL) {
        errx(-1, "cannot create local");
    }
    ak_debug("alloc local: %p\n", local);

    /* call */
    vm->cont->noret = 1;            /* drop return value */
    ak_vm_push(vm, local);

    vm->gc->state = AK_GC_START;    /* start gc */

    (void) ak_vm_call(vm, 0);

    ak_assert(vm->cont == &vm->conts[0]);
    ak_assert(vm->stacktop == 0);

    ak_debug("> --- exiting vm\n");

    vm->global = NULL;
    ak_vm_full_gc(vm);
    ak_vm_free(vm);
}

#if (AK_DEBUG_GPERF)
int ProfilerStart(char *fname);
void ProfilerStop(void);
void ProfilerRegisterThread(void);
#endif

int main(int argc, char **argv)
{
    struct ak_lexer *lex;
    int fd;
    if (argc != 2) {
        errx(-1, "Usage: %s <filename>", argv[0]);
    }
    fd = open(argv[1], O_RDONLY);
    if (fd == -1) {
        errx(-1, "open(%s) failed", argv[1]);
    }
    struct stat statbuf;
    if (fstat(fd, &statbuf) == -1) {
        errx(-1, "fstat(%d) failed", fd);
    }
    size_t len = statbuf.st_size;
    char *str = ak_alloc(len);
    if (str == NULL) {
        errx(-1, "ak_alloc(%zu) failed", len);
    }
    if (read(fd, str, len) != len) {
        errx(-1, "read() failed");
    }
#if 0
    lex = ak_lex_strn(str, len);
    if (lex == NULL) {
        errx(-1, "ak_lex_from_str() failed");
    }
    ak_lex_debug_token(lex);
    return 0;
#endif
#if (AK_DEBUG_GPERF)
    char *profile = "./ak.profile";
    if (ProfilerStart(profile)) {
        printf("ProfileStart(%s) ok\n", profile);
    } else {
        printf("ProfileStart(%s) failed\n", profile);
    }
#endif
#if 0
    struct timeval tv, tv2;
    gettimeofday (&tv, NULL);
#endif
    ak_main(str, len);
#if 0
    gettimeofday (&tv2, NULL);
    if(tv.tv_usec > tv2.tv_usec)    {
        tv2.tv_sec--;
        tv2.tv_usec += 1000000;
    }
    printf("\n>>> consume: %d.%06ds\n", (tv2.tv_sec - tv.tv_sec), (tv2.tv_usec - tv.tv_usec));
#endif
#if (AK_DEBUG_GPERF)
    ProfilerStop();
#endif
    ak_free(str);
    close(fd);
    return 0;
}
