#ifndef AK_CODE_H
#define AK_CODE_H

#include <stdint.h>
#include <ak_object.h>

struct ak_lexer;
struct ak_vm;
struct ak_table;

#if FOR_CODE_REVIEW /* for code review */
#define AK_OPC_GET      0x00000001
#define AK_OPC_SET      0x00000002
#define AK_OPC_GETC     0x00000003
#define AK_OPC_LIST     0x00000004
#define AK_OPC_TABLE    0x00000005
#define AK_OPC_CALL     0x00000006  /* operand: BIT 0~14:arguments number, BIT 15: pop return value flag */
#define AK_OPC_ADD      0x00000007
#define AK_OPC_SUB      0x00000008
#define AK_OPC_MUL      0x00000009
#define AK_OPC_DIV      0x0000000a
#define AK_OPC_MOD      0x0000000b
#define AK_OPC_BAND     0x0000000c
#define AK_OPC_BXOR     0x0000000d
#define AK_OPC_BOR      0x0000000e
#define AK_OPC_BSHL     0x0000000f
#define AK_OPC_BSHR     0x00000010
#define AK_OPC_EQ       0x00000011
#define AK_OPC_NEQ      0x00000012
#define AK_OPC_GT       0x00000013
#define AK_OPC_GE       0x00000014
#define AK_OPC_LT       0x00000015
#define AK_OPC_LE       0x00000016
#define AK_OPC_AND      0x00000017
#define AK_OPC_OR       0x00000018
#define AK_OPC_JMP      0x00000019  /* jump forward */
#define AK_OPC_JMPF     0x0000001a  /* jump forward if top[-1] is False */
#define AK_OPC_JMPB     0x0000001b  /* jump back */
#define AK_OPC_RET      0x0000001c
#define AK_OPC_GETV     0x0000001d  /* get local variable */
#define AK_OPC_SETV     0x0000001e  /* set local variable */
#define AK_OPC_GETUP    0x0000001f  /* get upvalue */
#define AK_OPC_SETUP    0x00000020  /* set upvalue */
#define AK_OPC_CLOSURE  0x00000021
#endif

#define AK_OPC_DEF(_) \
    _(GET) \
    _(SET) \
    _(GETC) \
    _(LIST) \
    _(TABLE) \
    _(CALL) \
    _(ADD) \
    _(SUB) \
    _(MUL) \
    _(DIV) \
    _(MOD) \
    _(BAND) \
    _(BXOR) \
    _(BOR) \
    _(BSHL) \
    _(BSHR) \
    _(EQ) \
    _(NEQ) \
    _(GT) \
    _(GE) \
    _(LT) \
    _(LE) \
    _(AND) \
    _(OR) \
    _(JMP) \
    _(JMPF) \
    _(JMPB) \
    _(RET) \
    _(GETV) \
    _(SETV) \
    _(GETUP) \
    _(SETUP) \
    _(CLOSURE)

enum {
#define AK_OPC_ENUM(name) AK_OPC_##name,
AK_OPC_DEF(AK_OPC_ENUM)
#undef AK_OPC_ENUM
    AK_OPC__MAX
} AK_OPC_OP;

#define AK_OPC_MASK     0x0000ffff
#define AK_OPR_MASK     0xffff0000

#define AK_OPR_CALL_NORET   0x8000  /* BIT 15 of CALL operand: no return value flag */

#define AK_OPCODE_EMIT(opc, opr) ((uint32_t) (opc) | ((uint32_t) (opr) << 16))
#define AK_OPCODE_OPC(opcode) ((uint32_t) (opcode) & AK_OPC_MASK)
#define AK_OPCODE_OPR(opcode) (((uint32_t) (opcode)) >> 16)

#define AK_MAX_OPCODE   1024
#define AK_MAX_CONSTANT 1024
#define AK_MAX_VAR      1024
#define AK_MAX_ARG      256
#define AK_MAX_UPVALUE  256     /* maximum of 8-bit index */

struct ak_code {
    AK_OBJECT_HEAD;

    uint32_t opcode[AK_MAX_OPCODE];
    int opnum;
    void *constant[AK_MAX_CONSTANT];
    int constnum;
    int argnum;
    /* TODO: uint32_t -> uint16_t */
    uint32_t upvalidx[AK_MAX_UPVALUE];   /* bit0-7 index, bit7-16 level */
                                         /* upvalue location is continuation[-level].vars[index] */
    int upvalnum;
    int varnum;

    /* TODO: delete following fields, which are only used during code emitting */
    struct ak_code *back;       /* who create this function */
    struct ak_table *variable;  /* key: variable name, value: variable index as C integer (should not gc) */
    struct ak_table *upvalue;   /* key: variable name, value: upvalue index as C integer  (shoudl not gc) */

    int syntax_error;
    char *syntax_errmsg;
    int breakidx;
    int whilesidx;
    int whilelevel;
    struct ak_object *varsaved; /* saved VAR for assign-stmt left value */
};

/* AK API: */
struct ak_code *ak_vm_code(struct ak_vm *vm);
void ak_code_free(struct ak_code *code);
struct ak_code *ak_code_lex(struct ak_lexer *lex);
void ak_code_debug(struct ak_lexer *lex, struct ak_code *code);
void ak_code_debug_opcode(struct ak_code *code, int pc, char *prefix);

/* C function code type */
typedef int (*ak_cfunc_t)(struct ak_vm *, int);

struct ak_cfunc {
    AK_OBJECT_HEAD;
    ak_cfunc_t f;
};

/* AK API: */
struct ak_cfunc *ak_vm_cfunc(struct ak_vm *vm, ak_cfunc_t f);
char *ak_opc2str(uint32_t opc);

#endif  /* AK_CODE_H */
