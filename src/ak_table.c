#include <ak_alloc.h>
#include <ak_gc.h>
#include <ak_object.h>
#include <ak_table.h>
#include <ak_number.h>
#include <ak_string.h>
#include <ak_vm.h>
#include <ak_debug.h>

#include <err.h>
#include <string.h>

static void __ak_table_set(struct ak_table *t, struct ak_object *key, struct ak_object *value);

int akc_hash(struct ak_object *obj)
{
    if (obj->type == AK_OBJ_NUM) {
        return (int) ak_num2d(obj);
    } else if (obj->type == AK_OBJ_STR) {
        return akc_string_hash((struct ak_string *) obj);
    } else {
        return 0;
    }
}

struct ak_table *ak_vm_table(struct ak_vm *vm)
{
    struct ak_table *t;

    ak_vm_gc_check(vm);

    t = (struct ak_table *) ak_gc_alloc_object(vm->gc, sizeof(struct ak_table));

    if (t != NULL) {
        t->type = AK_OBJ_TABLE;
        t->num = 0;
        t->maxnum = AK_TABLE_DEFAULT_SIZE;
        t->array = ak_calloc(sizeof(struct ak_table_entry) * AK_TABLE_DEFAULT_SIZE);
        if (t->array == NULL) {
            ak_free(t);
            t = NULL;
        }
    }

    return t;
}

#if 1
/* TODO: delete this, used by ak_code::upvalues/variable */

struct ak_table *ak_table(void)
{
    struct ak_table *t = ak_calloc(sizeof(struct ak_table));
    if (t != NULL) {
        t->type = AK_OBJ_TABLE;
        t->num = 0;
        t->maxnum = AK_TABLE_DEFAULT_SIZE;
        t->array = ak_calloc(sizeof(struct ak_table_entry) * AK_TABLE_DEFAULT_SIZE);
        if (t->array == NULL) {
            ak_free(t);
            t = NULL;
        }
    }
    return t;
}

void ak_table_free_string_keys(struct ak_table *t)
{
    struct ak_table_entry *e;
    int i;

    for (i = 0; i < t->maxnum; i++) {
        e = &t->array[i];
        if (e->key != NULL) {
            ak_assert(e->key->type == AK_OBJ_STR);
            ak_free(e->key);
            e->key = NULL;
        }
    }
}
#endif

void ak_table_free(struct ak_table *t)
{
#if (AK_DEBUG)
    memset(t->array, 0, sizeof(struct ak_table_entry) * t->maxnum);
#endif
    ak_free(t->array);
#if (AK_DEBUG)
    memset(t, 0, sizeof(struct ak_table));
#endif
    ak_free(t);
}

void ak_table_resize(struct ak_table *t)
{
    struct ak_table_entry *e, *old;
    int i, maxnum;
    e = ak_calloc(sizeof(struct ak_table_entry) * t->maxnum * 2);
    if (e == NULL) {
        errx(-1, "resize table failed");
    }
    old = t->array;
    maxnum = t->maxnum;
    /* construct new table */
    t->num = 0;
    t->maxnum = t->maxnum * 2;
    t->array = e;
    for (i = 0; i < maxnum; i++) {
        if (old[i].key != NULL) {
            __ak_table_set(t, old[i].key, old[i].value);
        }
    }
    ak_free(old);
}

struct ak_table_entry *ak_table_get_entry(struct ak_table *t, struct ak_object *key)
{
    struct ak_table_entry *e, *empty;
    int idx;

    empty = NULL;
    idx = akc_hash(key);

    while (1) {
        e = &t->array[idx % t->maxnum];
        if (e->key == NULL) {
            /* end */
            if (e->value == NULL) {
                break;
            }
            /* deleted */
            if (empty == NULL) {
                empty = e;
            }
        } else if (akc_equal(key, e->key)) {
            /* found */
            return e;
        }
        idx += 2;
    }
    return (empty == NULL) ?  e : empty;
}

static void __ak_table_set(struct ak_table *t, struct ak_object *key, struct ak_object *value)
{
    struct ak_table_entry *e = ak_table_get_entry(t, key);
    e->key = key;
    e->value = value;
}

void ak_table_set(struct ak_table *t, struct ak_object *key, struct ak_object *value)
{
    struct ak_table_entry *e;
    if (t->num >= t->maxnum / 2) {
        ak_table_resize(t);
    }
    e = ak_table_get_entry(t, key);
    e->key = key;
    e->value = value;
}

void ak_vm_table_set(struct ak_vm *vm, struct ak_table *t, struct ak_object *key, struct ak_object *value)
{
    int n = t->maxnum;
    ak_table_set(t, key, value);
    if (n != t->maxnum) {
        vm->gc->total += sizeof(struct ak_table_entry) * (t->maxnum - n);
    }
    ak_gc_write_barrier(vm->gc, (struct ak_object *) t, key);
    ak_gc_write_barrier(vm->gc, (struct ak_object *) t, value);
}

struct ak_object *ak_table_get(struct ak_table *t, struct ak_object *key)
{
    struct ak_table_entry *e = ak_table_get_entry(t, key);
    if (e->value == (struct ak_object *) 0xdead) {
        return NULL;
    }
    return e->value;
}

struct ak_object *ak_table_get_kv(struct ak_table *t, struct ak_object *key, struct ak_object **okey)
{
    struct ak_table_entry *e = ak_table_get_entry(t, key);
    if (okey != NULL) {
        *okey = e->key;
    }
    if (e->value == (struct ak_object *) 0xdead) {
        return NULL;
    }
    return e->value;
}

void ak_table_delete(struct ak_table *t, struct ak_object *key)
{
    struct ak_table_entry *e = ak_table_get_entry(t, key);
    if (e->key != NULL) {
        e->key = NULL;
        e->value = (struct ak_object *) 0xdead;
    } else {
        errx(-1, "try to delete non-existed kv");
    }
}
