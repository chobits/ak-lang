#include <ak_alloc.h>
#include <ak_object.h>
#include <ak_lex.h>
#include <ak_code.h>
#include <ak_vm.h>
#include <ak_list.h>
#include <ak_table.h>
#include <ak_number.h>
#include <ak_debug.h>
#include <ak_gc.h>

#include <err.h>
#include <stdio.h>
#include <string.h>


#define AK_VM_RUN_WITHOUT_RECURSION 1

static inline void ak_vm_save_cont(struct ak_vm *vm, uint32_t *pc, int noret);
static inline int ak_vm_pre_call(struct ak_vm *vm, int argnum);
static inline int ak_vm_post_call(struct ak_vm *vm, int retnum);

int ak_is_false(struct ak_object *obj)
{
    if (obj->type == AK_OBJ_NUM) {
        return ak_num2d(obj) == 0;
    } else if (obj->type == AK_OBJ_STR) {
        return ((struct ak_string *) obj)->len == 0;
    } else if (obj->type == AK_OBJ_LIST) {
        return ((struct ak_list *) obj)->num == 0;
    } else if (obj->type == AK_OBJ_TABLE) {
        return ((struct ak_table *) obj)->num == 0;
    } else if (obj->type == AK_OBJ_CODE) {
        return 0;
    } else {
        errx(-1, "vm: unknown object (type:%d)", obj->type);
    }
}

int akc_equal(struct ak_object *a, struct ak_object *b)
{
    if (a == b) {
        return 1;
    }
    if (a->type != b->type) {
        return 0;
    }
    if (a->type == AK_OBJ_NUM) {
        return ak_num2d(a) == ak_num2d(b);
    } else if (a->type == AK_OBJ_STR) {
        return akc_string_equal((struct ak_string *) a, (struct ak_string *) b);
    } else if (a->type == AK_OBJ_LIST) {
        errx(-1, "vm: list not implemented: %s", __func__);
    } else if (a->type == AK_OBJ_TABLE) {
        errx(-1, "vm: table not implemented: %s", __func__);
    } else if (a->type == AK_OBJ_CODE) {
        return a == b;
    } else {
        errx(-1, "vm: unknown object (type:%d)", a->type);
    }
    return 1;
}

struct ak_object *ak_get(struct ak_object *t, struct ak_object *key)
{
    if (t->type == AK_OBJ_TABLE) {
        return ak_table_get((struct ak_table *) t, key);
    } else if (t->type == AK_OBJ_LIST) {
        return ak_list_get((struct ak_list *) t, key);
    } else {
        errx(-1, "vm: try to get from %s object", ak_type2str(t->type));
    }
}

void ak_vm_set(struct ak_vm *vm, struct ak_object *t, struct ak_object *key, struct ak_object *value)
{
    if (t->type == AK_OBJ_TABLE) {
        ak_vm_table_set(vm, (struct ak_table *) t, key, value);
    } else if (t->type == AK_OBJ_LIST) {
        ak_vm_list_set(vm, (struct ak_list *) t, key, value);
    } else {
        errx(-1, "vm: try to set <key,value> to %s object", ak_type2str(t->type));
    }
}

struct ak_object *ak_vm_add(struct ak_vm *vm, struct ak_object *a, struct ak_object *b)
{
    if (a->type == AK_OBJ_NUM && b->type == AK_OBJ_NUM) {
        return (struct ak_object *) ak_vm_number_add(vm, (struct ak_number *) a, (struct ak_number *) b);
    } else if (a->type == AK_OBJ_STR && b->type == AK_OBJ_STR) {
        return (struct ak_object *) ak_vm_string_concat(vm, (struct ak_string *) a, (struct ak_string *) b);
    } else if (a->type == AK_OBJ_LIST) {
        errx(-1, "vm: list doest not support add operator");
    } else if (a->type == AK_OBJ_TABLE) {
        errx(-1, "vm: table doest not support add operator");
    } else if (a->type == AK_OBJ_CODE) {
        errx(-1, "vm: code doest not support add operator");
    } else {
        errx(-1, "vm: unknown object (type:%d)", a->type);
    }
}

struct ak_object *ak_vm_sub(struct ak_vm *vm, struct ak_object *a, struct ak_object *b)
{
    if (a->type == AK_OBJ_NUM && b->type == AK_OBJ_NUM) {
        return (struct ak_object *) ak_vm_number_sub(vm, (struct ak_number *) a, (struct ak_number *) b);
    } else if (a->type == AK_OBJ_STR && b->type == AK_OBJ_STR) {
        errx(-1, "vm: string doest not support sub operator");
    } else if (a->type == AK_OBJ_LIST) {
        errx(-1, "vm: list doest not support sub operator");
    } else if (a->type == AK_OBJ_TABLE) {
        errx(-1, "vm: table doest not support sub operator");
    } else if (a->type == AK_OBJ_CODE) {
        errx(-1, "vm: code doest not support sub operator");
    } else {
        errx(-1, "vm: unknown object (type:%d)", a->type);
    }
    return NULL;
}

struct ak_object *ak_vm_mul(struct ak_vm *vm, struct ak_object *a, struct ak_object *b)
{
    if (a->type == AK_OBJ_NUM && b->type == AK_OBJ_NUM) {
        return (struct ak_object *) ak_vm_number_mul(vm, (struct ak_number *) a, (struct ak_number *) b);
    } else if (a->type == AK_OBJ_STR && b->type == AK_OBJ_STR) {
        errx(-1, "vm: string doest not support mul operator");
    } else if (a->type == AK_OBJ_LIST) {
        errx(-1, "vm: list doest not support mul operator");
    } else if (a->type == AK_OBJ_TABLE) {
        errx(-1, "vm: table doest not support mul operator");
    } else if (a->type == AK_OBJ_CODE) {
        errx(-1, "vm: code doest not support mul operator");
    } else {
        errx(-1, "vm: unknown object (type:%d)", a->type);
    }
    return NULL;
}

struct ak_object *ak_vm_div(struct ak_vm *vm, struct ak_object *a, struct ak_object *b)
{
    if (a->type == AK_OBJ_NUM && b->type == AK_OBJ_NUM) {
        return (struct ak_object *) ak_vm_number_div(vm, (struct ak_number *) a, (struct ak_number *) b);
    } else if (a->type == AK_OBJ_STR && b->type == AK_OBJ_STR) {
        errx(-1, "vm: string doest not support div operator");
    } else if (a->type == AK_OBJ_LIST) {
        errx(-1, "vm: list doest not support div operator");
    } else if (a->type == AK_OBJ_TABLE) {
        errx(-1, "vm: table doest not support div operator");
    } else if (a->type == AK_OBJ_CODE) {
        errx(-1, "vm: code doest not support div operator");
    } else {
        errx(-1, "vm: unknown object (type:%d)", a->type);
    }
    return NULL;
}

struct ak_object *ak_vm_mod(struct ak_vm *vm, struct ak_object *a, struct ak_object *b)
{
    if (a->type == AK_OBJ_NUM && b->type == AK_OBJ_NUM) {
        return (struct ak_object *) ak_vm_number_mod(vm, (struct ak_number *) a, (struct ak_number *) b);
    } else if (a->type == AK_OBJ_STR && b->type == AK_OBJ_STR) {
        errx(-1, "vm: string doest not support mod operator");
    } else if (a->type == AK_OBJ_LIST) {
        errx(-1, "vm: list doest not support mod operator");
    } else if (a->type == AK_OBJ_TABLE) {
        errx(-1, "vm: table doest not support mod operator");
    } else if (a->type == AK_OBJ_CODE) {
        errx(-1, "vm: code doest not support mod operator");
    } else {
        errx(-1, "vm: unknown object (type:%d)", a->type);
    }
    return NULL;
}

struct ak_object *ak_band(struct ak_object *a, struct ak_object *b)
{
    struct ak_object *res;
    errx(-1, "%s: not implemented", __func__);
    return res;
}

struct ak_object *ak_bxor(struct ak_object *a, struct ak_object *b)
{
    struct ak_object *res;
    errx(-1, "%s: not implemented", __func__);
    return res;
}

struct ak_object *ak_bor(struct ak_object *a, struct ak_object *b)
{
    struct ak_object *res;
    errx(-1, "%s: not implemented", __func__);
    return res;
}

struct ak_object *ak_bshl(struct ak_object *a, struct ak_object *b)
{
    struct ak_object *res;
    errx(-1, "%s: not implemented", __func__);
    return res;
}

struct ak_object *ak_bshr(struct ak_object *a, struct ak_object *b)
{
    struct ak_object *res;
    errx(-1, "%s: not implemented", __func__);
    return res;
}

struct ak_object *ak_vm_eq(struct ak_vm *vm, struct ak_object *a, struct ak_object *b)
{
    struct ak_object *res = NULL;
    if (a->type == AK_OBJ_NUM) {
        if (akc_number_compare((struct ak_number *) a, (struct ak_number *) b) == 0) {
            return (struct ak_object *) ak_vm_number(vm, 1.0);
        } else {
            return (struct ak_object *) ak_vm_number(vm, 0.0);
        }
    } else if (a->type == AK_OBJ_STR) {
        if (akc_string_compare((struct ak_string *) a, (struct ak_string *) b) == 0) {
            return (struct ak_object *) ak_vm_number(vm, 1.0);
        } else {
            return (struct ak_object *) ak_vm_number(vm, 0.0);
        }
    } else if (a->type == AK_OBJ_LIST) {
        errx(-1, "vm: list doest not support lt operator");
    } else if (a->type == AK_OBJ_TABLE) {
        errx(-1, "vm: table doest not support lt operator");
    } else if (a->type == AK_OBJ_CODE) {
        errx(-1, "vm: code doest not support lt operator");
    } else {
        errx(-1, "vm: unknown object (type:%d)", a->type);
    }
    return res;
}

struct ak_object *ak_vm_neq(struct ak_vm *vm, struct ak_object *a, struct ak_object *b)
{
    struct ak_object *res = NULL;
    if (a->type == AK_OBJ_NUM) {
        if (akc_number_compare((struct ak_number *) a, (struct ak_number *) b) == 0) {
            return (struct ak_object *) ak_vm_number(vm, 0.0);
        } else {
            return (struct ak_object *) ak_vm_number(vm, 1.0);
        }
    } else if (a->type == AK_OBJ_STR) {
        if (akc_string_compare((struct ak_string *) a, (struct ak_string *) b) == 0) {
            return (struct ak_object *) ak_vm_number(vm, 0.0);
        } else {
            return (struct ak_object *) ak_vm_number(vm, 1.0);
        }
    } else if (a->type == AK_OBJ_LIST) {
        errx(-1, "vm: list doest not support lt operator");
    } else if (a->type == AK_OBJ_TABLE) {
        errx(-1, "vm: table doest not support lt operator");
    } else if (a->type == AK_OBJ_CODE) {
        errx(-1, "vm: code doest not support lt operator");
    } else {
        errx(-1, "vm: unknown object (type:%d)", a->type);
    }
    return res;
}

struct ak_object *ak_vm_gt(struct ak_vm *vm, struct ak_object *a, struct ak_object *b)
{
    struct ak_object *res = NULL;
    if (a->type == AK_OBJ_NUM) {
        if (akc_number_compare((struct ak_number *) a, (struct ak_number *) b) > 0) {
            return (struct ak_object *) ak_vm_number(vm, 1.0);
        } else {
            return (struct ak_object *) ak_vm_number(vm, 0.0);
        }
    } else if (a->type == AK_OBJ_STR) {
        if (akc_string_compare((struct ak_string *) a, (struct ak_string *) b) > 0) {
            return (struct ak_object *) ak_vm_number(vm, 1.0);
        } else {
            return (struct ak_object *) ak_vm_number(vm, 0.0);
        }
    } else if (a->type == AK_OBJ_LIST) {
        errx(-1, "vm: list doest not support lt operator");
    } else if (a->type == AK_OBJ_TABLE) {
        errx(-1, "vm: table doest not support lt operator");
    } else if (a->type == AK_OBJ_CODE) {
        errx(-1, "vm: code doest not support lt operator");
    } else {
        errx(-1, "vm: unknown object (type:%d)", a->type);
    }
    return res;
}

struct ak_object *ak_vm_ge(struct ak_vm *vm, struct ak_object *a, struct ak_object *b)
{
    struct ak_object *res = NULL;
    if (a->type == AK_OBJ_NUM) {
        if (akc_number_compare((struct ak_number *) a, (struct ak_number *) b) >= 0) {
            return (struct ak_object *) ak_vm_number(vm, 1.0);
        } else {
            return (struct ak_object *) ak_vm_number(vm, 0.0);
        }
    } else if (a->type == AK_OBJ_STR) {
        if (akc_string_compare((struct ak_string *) a, (struct ak_string *) b) >= 0) {
            return (struct ak_object *) ak_vm_number(vm, 1.0);
        } else {
            return (struct ak_object *) ak_vm_number(vm, 0.0);
        }
    } else if (a->type == AK_OBJ_LIST) {
        errx(-1, "vm: list doest not support lt operator");
    } else if (a->type == AK_OBJ_TABLE) {
        errx(-1, "vm: table doest not support lt operator");
    } else if (a->type == AK_OBJ_CODE) {
        errx(-1, "vm: code doest not support lt operator");
    } else {
        errx(-1, "vm: unknown object (type:%d)", a->type);
    }
    return res;
}

struct ak_object *ak_vm_lt(struct ak_vm *vm, struct ak_object *a, struct ak_object *b)
{
    struct ak_object *res = NULL;
    if (a->type == AK_OBJ_NUM) {
        if (akc_number_compare((struct ak_number *) a, (struct ak_number *) b) < 0) {
            return (struct ak_object *) ak_vm_number(vm, 1.0);
        } else {
            return (struct ak_object *) ak_vm_number(vm, 0.0);
        }
    } else if (a->type == AK_OBJ_STR) {
        if (akc_string_compare((struct ak_string *) a, (struct ak_string *) b) < 0) {
            return (struct ak_object *) ak_vm_number(vm, 1.0);
        } else {
            return (struct ak_object *) ak_vm_number(vm, 0.0);
        }
    } else if (a->type == AK_OBJ_LIST) {
        errx(-1, "vm: list doest not support lt operator");
    } else if (a->type == AK_OBJ_TABLE) {
        errx(-1, "vm: table doest not support lt operator");
    } else if (a->type == AK_OBJ_CODE) {
        errx(-1, "vm: code doest not support lt operator");
    } else {
        errx(-1, "vm: unknown object (type:%d)", a->type);
    }
    return res;
}

struct ak_object *ak_vm_le(struct ak_vm *vm, struct ak_object *a, struct ak_object *b)
{
    struct ak_object *res = NULL;
    if (a->type == AK_OBJ_NUM) {
        if (akc_number_compare((struct ak_number *) a, (struct ak_number *) b) <= 0) {
            return (struct ak_object *) ak_vm_number(vm, 1.0);
        } else {
            return (struct ak_object *) ak_vm_number(vm, 0.0);
        }
    } else if (a->type == AK_OBJ_STR) {
        if (akc_string_compare((struct ak_string *) a, (struct ak_string *) b) <= 0) {
            return (struct ak_object *) ak_vm_number(vm, 1.0);
        } else {
            return (struct ak_object *) ak_vm_number(vm, 0.0);
        }
    } else if (a->type == AK_OBJ_LIST) {
        errx(-1, "vm: list doest not support lt operator");
    } else if (a->type == AK_OBJ_TABLE) {
        errx(-1, "vm: table doest not support lt operator");
    } else if (a->type == AK_OBJ_CODE) {
        errx(-1, "vm: code doest not support lt operator");
    } else {
        errx(-1, "vm: unknown object (type:%d)", a->type);
    }
    return res;
}

struct ak_object *ak_and(struct ak_object *a, struct ak_object *b)
{
    if (ak_is_false(a)) {
        return a;
    }
    return b;
}

struct ak_object *ak_or(struct ak_object *a, struct ak_object *b)
{
    if (!ak_is_false(a)) {
        return a;
    }
    return b;
}

/* AK API: */
void ak_vm_closure(struct ak_vm *vm, struct ak_local *local)
{
    int i, index, level;
    struct ak_code *code = local->code;

#if (AK_DEBUG)
    if (code->type != AK_OBJ_CODE) {
        errx(-1, "ak_vm_closure: not code, it is %s", ak_type2str(code->type));
    }
#endif

    /* initialize local upvalues */

    for (i = 0; i < code->upvalnum; i++) {
        index = code->upvalidx[i];
        level = (index >> 8);
        index = index & 0xff;
        local->upvals[i] = vm->conts[vm->funclevel - level].vars[index];
        if (local->upvals[i] == NULL) { /* current closure has not been SETUP */
            local->upvals[i] = (struct ak_object *) local;
        }
    }
}

/* AK API: */
struct ak_vm *ak_vm(void)
{
    struct ak_vm *vm = ak_calloc(sizeof(struct ak_vm));
    if (vm != NULL) {
        vm->gc = ak_gc();       /* TODO: make gc in ak_vm */
        if (vm->gc == NULL) {
            ak_free(vm);
            return NULL;
        }
        vm->global = ak_vm_table(vm);
        if (vm->global == NULL) {
            ak_free(vm->gc);
            ak_free(vm);
            return NULL;
        }
        /* first cont */
        vm->funclevel = 0;
        vm->cont = &vm->conts[vm->funclevel++];
    }
    return vm;
}

void ak_vm_free(struct ak_vm *vm)
{
    ak_gc_free(vm->gc);
    vm->gc = NULL;

    ak_free(vm);
}

/* AK API: */
void ak_vm_push(struct ak_vm *vm, void *obj)
{
    if (vm->stacktop >= AK_VM_MAX_STACK) {
        errx(-1, "vm: stack is overflow");
    }
    vm->stack[vm->stacktop++] = obj;
    /* TODO: whiten the object when object is new alloced */
    /* TODO: ak_gc_write_barrier(vm->gc, vm->stack, obj); */

    /* write barrier: forward */
    ak_gc_mark_gray(vm->gc, obj);
}

/* AK API: */
void ak_vm_popn(struct ak_vm *vm, int n)
{
    if (vm->stacktop < n) {
        errx(-1, "vm: try to pop %d elements from %d-elements stack", n, vm->stacktop);
    }
    vm->stacktop -= n;
}

/* AK API: */
void *ak_vm_pop(struct ak_vm *vm)
{
    if (vm->stacktop == 0) {
        errx(-1, "vm: try to pop element from empty stack");
    }
    return vm->stack[--vm->stacktop];
}

/* AK API: */
void ak_vm_set_top(struct ak_vm *vm, int top, void *obj)
{
    if (vm->stacktop + top < 0 || top >= 0) {
        errx(-1, "vm: try to set element out of range in stack");
    }

    vm->stack[vm->stacktop + top] = obj;
    ak_gc_mark_gray(vm->gc, obj);
}

/* AK API: */
void *ak_vm_top(struct ak_vm *vm, int top)
{
    if (vm->stacktop + top < 0 || top >= 0) {
        errx(-1, "vm: try to get element out of range");
    }
    return vm->stack[vm->stacktop + top];
}

#if 0
/* AK API: */
int ak_vm_run(struct ak_vm *vm)
{
    struct ak_object *obj, *obja, *objb;
    struct ak_object **vars, **upvals;
    struct ak_code *code;
    struct ak_local *local;
    uint32_t opc, opr;
    int pc, i;

vm_run:

    ak_debug("vm: --- run func level: %d ---\n", vm->funclevel);

    if (vm->funclevel >= AK_VM_MAX_FUNCLEVEL) {
        errx(-1, "vm: function level %d over the limit %d", vm->funclevel, AK_VM_MAX_FUNCLEVEL);
    }

    vars = vm->cont->vars;
    local = vm->cont->local;
    pc = vm->cont->pc;
    upvals = local->upvals;
    code = local->code;

    while (pc < code->opnum) {
#if (AK_DEBUG)
        ak_code_debug_opcode(code, pc, "vm:");
#endif
        opc = AK_OPCODE_OPC(code->opcode[pc]);
        opr = AK_OPCODE_OPR(code->opcode[pc]);
        pc++;   /* pc always points next opcode */
        switch (opc) {
            case AK_OPC_GET:
                obja = ak_vm_pop(vm);   /* key */
                if (opr == 2) {
                    obj = ak_get(ak_vm_pop(vm), obja);
                } else {
                    obj = ak_get((struct ak_object *) vm->global, obja);
                }
                if (obj == NULL) {
                    ak_object_debug("vm: key is", obja);
                    errx(-1, "vm: get table[key] failed");
                }
                ak_vm_push(vm, obj);
                break;
            case AK_OPC_SET:
                objb = ak_vm_pop(vm);   /* value */
                obja = ak_vm_pop(vm);   /* key */
                obj = (opr == 3) ? ak_vm_pop(vm) : vm->global;
                ak_vm_set(vm, obj, obja, objb);
                break;
            case AK_OPC_GETC:
                if (opr >= code->constnum) {
                    errx(-1, "vm: try to get constant out of range");
                }
                ak_vm_push(vm, code->constant[opr]);
                break;
            case AK_OPC_LIST:
                obj = (struct ak_object *) ak_vm_list(vm);
                if (obj == NULL) {
                    errx(-1, "vm: cannot create list");
                }
                for (i = -opr; i < 0; i++) {
                    ak_vm_list_append(vm, (struct ak_list *) obj, ak_vm_top(vm, i));
                }
                vm->stacktop -= opr;
                ak_vm_push(vm, obj);
                break;
            case AK_OPC_TABLE:
                obj = (struct ak_object *) ak_vm_table(vm);
                if (obj == NULL) {
                    errx(-1, "vm: cannot create table");
                }
                for (i = -opr; i < 0; i += 2) {
                    ak_vm_table_set(vm, (struct ak_table *) obj, ak_vm_top(vm, i), ak_vm_top(vm, i + 1));
                }
                vm->stacktop -= opr;
                ak_vm_push(vm, obj);
                break;
            case AK_OPC_ADD:
                obja = ak_vm_pop(vm);
                obj = ak_vm_pop(vm);
                obj = ak_vm_add(vm, obj, obja);
                if (obj == NULL) {
                    errx(-1, "vm: ak_add() failed");
                }
                ak_vm_push(vm, obj);
                break;
            case AK_OPC_SUB:
                obja = ak_vm_pop(vm);
                obj = ak_vm_pop(vm);
                obj = ak_vm_sub(vm, obj, obja);
                if (obj == NULL) {
                    errx(-1, "vm: ak_sub() failed");
                }
                ak_vm_push(vm, obj);
                break;
            case AK_OPC_MUL:
                obja = ak_vm_pop(vm);
                obj = ak_vm_pop(vm);
                obj = ak_vm_mul(vm, obj, obja);
                if (obj == NULL) {
                    errx(-1, "vm: ak_mul() failed");
                }
                ak_vm_push(vm, obj);
                break;
            case AK_OPC_DIV:
                obja = ak_vm_pop(vm);
                obj = ak_vm_pop(vm);
                obj = ak_vm_div(vm, obj, obja);
                if (obj == NULL) {
                    errx(-1, "vm: ak_div() failed");
                }
                ak_vm_push(vm, obj);
                break;
            case AK_OPC_MOD:
                obja = ak_vm_pop(vm);
                obj = ak_vm_pop(vm);
                obj = ak_vm_mod(vm, obj, obja);
                if (obj == NULL) {
                    errx(-1, "vm: ak_mod() failed");
                }
                ak_vm_push(vm, obj);
                break;
            case AK_OPC_BAND:
                obja = ak_vm_pop(vm);
                obj = ak_vm_pop(vm);
                obj = ak_band(obj, obja);
                if (obj == NULL) {
                    errx(-1, "vm: ak_band() failed");
                }
                ak_vm_push(vm, obj);
                break;
            case AK_OPC_BXOR:
                obja = ak_vm_pop(vm);
                obj = ak_vm_pop(vm);
                obj = ak_bxor(obj, obja);
                if (obj == NULL) {
                    errx(-1, "vm: ak_bxor() failed");
                }
                ak_vm_push(vm, obj);
                break;
            case AK_OPC_BOR:
                obja = ak_vm_pop(vm);
                obj = ak_vm_pop(vm);
                obj = ak_bor(obj, obja);
                if (obj == NULL) {
                    errx(-1, "vm: ak_bor() failed");
                }
                ak_vm_push(vm, obj);
                break;
            case AK_OPC_BSHL:
                obja = ak_vm_pop(vm);
                obj = ak_vm_pop(vm);
                obj = ak_bshl(obj, obja);
                if (obj == NULL) {
                    errx(-1, "vm: ak_bshl() failed");
                }
                ak_vm_push(vm, obj);
                break;
            case AK_OPC_BSHR:
                obja = ak_vm_pop(vm);
                obj = ak_vm_pop(vm);
                obj = ak_bshr(obj, obja);
                if (obj == NULL) {
                    errx(-1, "vm: ak_bshr() failed");
                }
                ak_vm_push(vm, obj);
                break;
            case AK_OPC_EQ:
                obja = ak_vm_pop(vm);
                obj = ak_vm_pop(vm);
                obj = ak_vm_eq(vm, obj, obja);
                if (obj == NULL) {
                    errx(-1, "vm: ak_vm_eq() failed");
                }
                ak_vm_push(vm, obj);
                break;
            case AK_OPC_NEQ:
                obja = ak_vm_pop(vm);
                obj = ak_vm_pop(vm);
                obj = ak_vm_neq(vm, obj, obja);
                if (obj == NULL) {
                    errx(-1, "vm: ak_vm_neq() failed");
                }
                ak_vm_push(vm, obj);
                break;
            case AK_OPC_GT:
                obja = ak_vm_pop(vm);
                obj = ak_vm_pop(vm);
                obj = ak_vm_gt(vm, obj, obja);
                if (obj == NULL) {
                    errx(-1, "vm: ak_vm_gt() failed");
                }
                ak_vm_push(vm, obj);
                break;
            case AK_OPC_GE:
                obja = ak_vm_pop(vm);
                obj = ak_vm_pop(vm);
                obj = ak_vm_ge(vm, obj, obja);
                if (obj == NULL) {
                    errx(-1, "vm: ak_vm_ge() failed");
                }
                ak_vm_push(vm, obj);
                break;
            case AK_OPC_LT:
                obja = ak_vm_pop(vm);
                obj = ak_vm_pop(vm);
                obj = ak_vm_lt(vm, obj, obja);
                if (obj == NULL) {
                    errx(-1, "vm: ak_vm_lt() failed");
                }
                ak_vm_push(vm, obj);
                break;
            case AK_OPC_LE:
                obja = ak_vm_pop(vm);
                obj = ak_vm_pop(vm);
                obj = ak_vm_le(vm, obj, obja);
                if (obj == NULL) {
                    errx(-1, "vm: ak_le() failed");
                }
                ak_vm_push(vm, obj);
                break;
            case AK_OPC_AND:
                obja = ak_vm_pop(vm);
                obj = ak_vm_pop(vm);
                obj = ak_and(obj, obja);
                if (obj == NULL) {
                    errx(-1, "vm: ak_and() failed");
                }
                ak_vm_push(vm, obj);
                break;
            case AK_OPC_OR:
                obja = ak_vm_pop(vm);
                obj = ak_vm_pop(vm);
                obj = ak_or(obj, obja);
                if (obj == NULL) {
                    errx(-1, "vm: ak_or() failed");
                }
                ak_vm_push(vm, obj);
                break;
            case AK_OPC_JMP:
                pc = pc - 1 + opr;
                break;
            case AK_OPC_JMPF:
                if (ak_is_false(ak_vm_pop(vm))) {
                    pc = pc - 1 + opr;
                }
                break;
            case AK_OPC_JMPB:
                pc = pc - 1 - opr;
                break;
            case AK_OPC_CALL:
                {
#if (AK_VM_RUN_WITHOUT_RECURSION)
                int retnum, noret;
                noret = !!(opr & AK_OPR_CALL_NORET);
                opr = opr & ~AK_OPR_CALL_NORET;

                ak_vm_save_cont(vm, pc, noret);

                retnum = ak_vm_pre_call(vm, opr);

                if (retnum > 0 && vm->cont->noret) {
                    ak_vm_popn(vm, retnum);
                }

                if (retnum < 0) {   /* call AK_OBJ_CODE */
                    goto vm_run;
                }
#else
                int retnum, noret;
                noret = !!(opr & AK_OPR_CALL_NORET);
                opr = opr & ~AK_OPR_CALL_NORET;

                ak_vm_save_cont(vm, pc, noret);

                retnum = ak_vm_call(vm, opr);

                if (retnum > 0 && vm->cont->noret) {
                    ak_vm_popn(vm, retnum);
                }
#endif
                break;
                }
            case AK_OPC_RET:
                {
                ak_debug("vm: --- out func level: %d ---\n", vm->funclevel);
#if (AK_VM_RUN_WITHOUT_RECURSION)
                int retnum = ak_vm_post_call(vm, (int) opr);

                if (retnum > 0 && vm->cont->noret) {
                    ak_vm_popn(vm, retnum);
                }

                if (vm->funclevel > 1) {
                    goto vm_run;
                }

                return retnum;
#else
                return (int) opr;
#endif
                break;
                }
            case AK_OPC_GETV:
                obj = vars[opr];
                if (obj == NULL) {
                    errx(-1, "vm: use non-existed local variable");
                }
                ak_vm_push(vm, obj);
                break;
            case AK_OPC_SETV:
                vars[opr] = ak_vm_pop(vm);
                break;
            case AK_OPC_GETUP:
                obj = upvals[opr];
                if (obj == NULL) {
                    errx(-1, "vm: use non-existed local upvalue");
                }
                ak_vm_push(vm, obj);
                break;
            case AK_OPC_SETUP:
                upvals[opr] = ak_vm_pop(vm);
                break;
            case AK_OPC_CLOSURE:
                obj = (struct ak_object *) ak_vm_local(vm, (struct ak_code *) ak_vm_pop(vm));
                if (obj == NULL) {
                    errx(-1, "vm: cannot create local for CLOSURE");
                }
                ak_vm_closure(vm, (struct ak_local *) obj);
                ak_vm_push(vm ,obj);
                break;
            default:
                errx(-1, "vm: unknown opcode %4u:%4u", opc, opr);
                break;
        }   /* end of switch */
    }   /* end of while */
    ak_debug("vm: --- out func level: %d ---\n", vm->funclevel);
    return 0;   /* no argument returned */
}

#else

/* referer to jvm/hotspot/src/share/vm/interpreter/bytecodeInterpreter.cpp */

/* AK API: */
int ak_vm_run(struct ak_vm *vm)
{
    struct ak_object *obj, *obja, *objb;
    struct ak_object **vars, **upvals;
    struct ak_code *code;
    struct ak_local *local;
    uint32_t opc, opr, *pc;
    int i;

    static void *dispatch_table[] = {
#define AK_OPC_LABEL(name) &&label_AK_OPC_##name,
AK_OPC_DEF(AK_OPC_LABEL)
#undef AK_OPC_LABEL
    };

vm_run:

    ak_debug("vm: --- run func level: %d ---\n", vm->funclevel);

    if (vm->funclevel >= AK_VM_MAX_FUNCLEVEL) {
        errx(-1, "vm: function level %d over the limit %d", vm->funclevel, AK_VM_MAX_FUNCLEVEL);
    }

    vars = vm->cont->vars;
    local = vm->cont->local;
    pc = vm->cont->pc;
    upvals = local->upvals;
    code = local->code;

#if (AK_DEBUG)
#define ak_code_debug_opcode_(code, pc, s) ak_code_debug_opcode(code, pc - code->opcode, s)
#else
#define ak_code_debug_opcode_(code, pc, s) ((void)0)
#endif

#define DISPATCH()                                  \
        ak_code_debug_opcode_(code, pc, "vm:");     \
        opc = AK_OPCODE_OPC(*pc);                   \
        opr = AK_OPCODE_OPR(*pc);                   \
        pc++;   /* pc always points next opcode */  \
        ak_assert(opc < AK_OPC__MAX);               \
        goto *dispatch_table[opc]

    while (1) {

    DISPATCH(); /* first dispatch */

            label_AK_OPC_GET:
                obja = ak_vm_pop(vm);   /* key */
                if (opr == 2) {
                    obj = ak_get(ak_vm_pop(vm), obja);
                } else {
                    obj = ak_get((struct ak_object *) vm->global, obja);
                }
                if (obj == NULL) {
                    ak_object_debug("vm: key is", obja);
                    errx(-1, "vm: get table[key] failed");
                }
                ak_vm_push(vm, obj);
                DISPATCH();
            label_AK_OPC_SET:
                objb = ak_vm_pop(vm);   /* value */
                obja = ak_vm_pop(vm);   /* key */
                obj = (opr == 3) ? ak_vm_pop(vm) : vm->global;
                ak_vm_set(vm, obj, obja, objb);
                DISPATCH();
            label_AK_OPC_GETC:
                if (opr >= code->constnum) {
                    errx(-1, "vm: try to get constant out of range");
                }
                ak_vm_push(vm, code->constant[opr]);
                DISPATCH();
            label_AK_OPC_LIST:
                obj = (struct ak_object *) ak_vm_list(vm);
                if (obj == NULL) {
                    errx(-1, "vm: cannot create list");
                }
                for (i = -opr; i < 0; i++) {
                    ak_vm_list_append(vm, (struct ak_list *) obj, ak_vm_top(vm, i));
                }
                vm->stacktop -= opr;
                ak_vm_push(vm, obj);
                DISPATCH();
            label_AK_OPC_TABLE:
                obj = (struct ak_object *) ak_vm_table(vm);
                if (obj == NULL) {
                    errx(-1, "vm: cannot create table");
                }
                for (i = -opr; i < 0; i += 2) {
                    ak_vm_table_set(vm, (struct ak_table *) obj, ak_vm_top(vm, i), ak_vm_top(vm, i + 1));
                }
                vm->stacktop -= opr;
                ak_vm_push(vm, obj);
                DISPATCH();
            label_AK_OPC_ADD:
                obja = ak_vm_pop(vm);
                obj = ak_vm_pop(vm);
                obj = ak_vm_add(vm, obj, obja);
                if (obj == NULL) {
                    errx(-1, "vm: ak_add() failed");
                }
                ak_vm_push(vm, obj);
                DISPATCH();
            label_AK_OPC_SUB:
                obja = ak_vm_pop(vm);
                obj = ak_vm_pop(vm);
                obj = ak_vm_sub(vm, obj, obja);
                if (obj == NULL) {
                    errx(-1, "vm: ak_sub() failed");
                }
                ak_vm_push(vm, obj);
                DISPATCH();
            label_AK_OPC_MUL:
                obja = ak_vm_pop(vm);
                obj = ak_vm_pop(vm);
                obj = ak_vm_mul(vm, obj, obja);
                if (obj == NULL) {
                    errx(-1, "vm: ak_mul() failed");
                }
                ak_vm_push(vm, obj);
                DISPATCH();
            label_AK_OPC_DIV:
                obja = ak_vm_pop(vm);
                obj = ak_vm_pop(vm);
                obj = ak_vm_div(vm, obj, obja);
                if (obj == NULL) {
                    errx(-1, "vm: ak_div() failed");
                }
                ak_vm_push(vm, obj);
                DISPATCH();
            label_AK_OPC_MOD:
                obja = ak_vm_pop(vm);
                obj = ak_vm_pop(vm);
                obj = ak_vm_mod(vm, obj, obja);
                if (obj == NULL) {
                    errx(-1, "vm: ak_mod() failed");
                }
                ak_vm_push(vm, obj);
                DISPATCH();
            label_AK_OPC_BAND:
                obja = ak_vm_pop(vm);
                obj = ak_vm_pop(vm);
                obj = ak_band(obj, obja);
                if (obj == NULL) {
                    errx(-1, "vm: ak_band() failed");
                }
                ak_vm_push(vm, obj);
                DISPATCH();
            label_AK_OPC_BXOR:
                obja = ak_vm_pop(vm);
                obj = ak_vm_pop(vm);
                obj = ak_bxor(obj, obja);
                if (obj == NULL) {
                    errx(-1, "vm: ak_bxor() failed");
                }
                ak_vm_push(vm, obj);
                DISPATCH();
            label_AK_OPC_BOR:
                obja = ak_vm_pop(vm);
                obj = ak_vm_pop(vm);
                obj = ak_bor(obj, obja);
                if (obj == NULL) {
                    errx(-1, "vm: ak_bor() failed");
                }
                ak_vm_push(vm, obj);
                DISPATCH();
            label_AK_OPC_BSHL:
                obja = ak_vm_pop(vm);
                obj = ak_vm_pop(vm);
                obj = ak_bshl(obj, obja);
                if (obj == NULL) {
                    errx(-1, "vm: ak_bshl() failed");
                }
                ak_vm_push(vm, obj);
                DISPATCH();
            label_AK_OPC_BSHR:
                obja = ak_vm_pop(vm);
                obj = ak_vm_pop(vm);
                obj = ak_bshr(obj, obja);
                if (obj == NULL) {
                    errx(-1, "vm: ak_bshr() failed");
                }
                ak_vm_push(vm, obj);
                DISPATCH();
            label_AK_OPC_EQ:
                obja = ak_vm_pop(vm);
                obj = ak_vm_pop(vm);
                obj = ak_vm_eq(vm, obj, obja);
                if (obj == NULL) {
                    errx(-1, "vm: ak_vm_eq() failed");
                }
                ak_vm_push(vm, obj);
                DISPATCH();
            label_AK_OPC_NEQ:
                obja = ak_vm_pop(vm);
                obj = ak_vm_pop(vm);
                obj = ak_vm_neq(vm, obj, obja);
                if (obj == NULL) {
                    errx(-1, "vm: ak_vm_neq() failed");
                }
                ak_vm_push(vm, obj);
                DISPATCH();
            label_AK_OPC_GT:
                obja = ak_vm_pop(vm);
                obj = ak_vm_pop(vm);
                obj = ak_vm_gt(vm, obj, obja);
                if (obj == NULL) {
                    errx(-1, "vm: ak_vm_gt() failed");
                }
                ak_vm_push(vm, obj);
                DISPATCH();
            label_AK_OPC_GE:
                obja = ak_vm_pop(vm);
                obj = ak_vm_pop(vm);
                obj = ak_vm_ge(vm, obj, obja);
                if (obj == NULL) {
                    errx(-1, "vm: ak_vm_ge() failed");
                }
                ak_vm_push(vm, obj);
                DISPATCH();
            label_AK_OPC_LT:
                obja = ak_vm_pop(vm);
                obj = ak_vm_pop(vm);
                obj = ak_vm_lt(vm, obj, obja);
                if (obj == NULL) {
                    errx(-1, "vm: ak_vm_lt() failed");
                }
                ak_vm_push(vm, obj);
                DISPATCH();
            label_AK_OPC_LE:
                obja = ak_vm_pop(vm);
                obj = ak_vm_pop(vm);
                obj = ak_vm_le(vm, obj, obja);
                if (obj == NULL) {
                    errx(-1, "vm: ak_le() failed");
                }
                ak_vm_push(vm, obj);
                DISPATCH();
            label_AK_OPC_AND:
                obja = ak_vm_pop(vm);
                obj = ak_vm_pop(vm);
                obj = ak_and(obj, obja);
                if (obj == NULL) {
                    errx(-1, "vm: ak_and() failed");
                }
                ak_vm_push(vm, obj);
                DISPATCH();
            label_AK_OPC_OR:
                obja = ak_vm_pop(vm);
                obj = ak_vm_pop(vm);
                obj = ak_or(obj, obja);
                if (obj == NULL) {
                    errx(-1, "vm: ak_or() failed");
                }
                ak_vm_push(vm, obj);
                DISPATCH();
            label_AK_OPC_JMP:
                pc = pc - 1 + opr;
                DISPATCH();
            label_AK_OPC_JMPF:
                if (ak_is_false(ak_vm_pop(vm))) {
                    pc = pc - 1 + opr;
                }
                DISPATCH();
            label_AK_OPC_JMPB:
                pc = pc - 1 - opr;
                DISPATCH();
            label_AK_OPC_CALL:
                {
#if (AK_VM_RUN_WITHOUT_RECURSION)
                int retnum, noret;
                noret = !!(opr & AK_OPR_CALL_NORET);
                opr = opr & ~AK_OPR_CALL_NORET;

                ak_vm_save_cont(vm, pc, noret);

                retnum = ak_vm_pre_call(vm, opr);

                if (retnum > 0 && vm->cont->noret) {
                    ak_vm_popn(vm, retnum);
                }

                if (retnum < 0) {   /* call AK_OBJ_CODE */
                    goto vm_run;
                }
#else
                int retnum, noret;
                noret = !!(opr & AK_OPR_CALL_NORET);
                opr = opr & ~AK_OPR_CALL_NORET;

                ak_vm_save_cont(vm, pc, noret);

                retnum = ak_vm_call(vm, opr);

                if (retnum > 0 && vm->cont->noret) {
                    ak_vm_popn(vm, retnum);
                }
#endif
                DISPATCH();
                }
            label_AK_OPC_RET:
                {
                ak_debug("vm: --- out func level: %d ---\n", vm->funclevel);
#if (AK_VM_RUN_WITHOUT_RECURSION)
                int retnum = ak_vm_post_call(vm, (int) opr);

                if (retnum > 0 && vm->cont->noret) {
                    ak_vm_popn(vm, retnum);
                }

                if (vm->funclevel > 1) {
                    goto vm_run;
                }

                return retnum;
#else
                return (int) opr;
#endif
                DISPATCH();
                }
            label_AK_OPC_GETV:
                obj = vars[opr];
                if (obj == NULL) {
                    errx(-1, "vm: use non-existed local variable");
                }
                ak_vm_push(vm, obj);
                DISPATCH();
            label_AK_OPC_SETV:
                vars[opr] = ak_vm_pop(vm);
                DISPATCH();
            label_AK_OPC_GETUP:
                obj = upvals[opr];
                if (obj == NULL) {
                    errx(-1, "vm: use non-existed local upvalue");
                }
                ak_vm_push(vm, obj);
                DISPATCH();
            label_AK_OPC_SETUP:
                upvals[opr] = ak_vm_pop(vm);
                DISPATCH();
            label_AK_OPC_CLOSURE:
                obj = (struct ak_object *) ak_vm_local(vm, (struct ak_code *) ak_vm_pop(vm));
                if (obj == NULL) {
                    errx(-1, "vm: cannot create local for CLOSURE");
                }
                ak_vm_closure(vm, (struct ak_local *) obj);
                ak_vm_push(vm ,obj);
                DISPATCH();
    } /* end of dispatch logic */

    ak_debug("vm: --- out func level: %d ---\n", vm->funclevel);
    return 0;   /* no argument returned */
}
#endif

/* AK API: */
struct ak_local *ak_vm_local(struct ak_vm *vm, struct ak_code *code)
{
    ak_assert(code->type == AK_OBJ_CODE || code->type == AK_OBJ_CFUNC);
    ak_vm_gc_check(vm);
    /* TODO: optimize logic of (code->type == AK_OBJ_CODE) */
    size_t size = (code->type == AK_OBJ_CODE) ? (sizeof(struct ak_object *) * code->upvalnum) : 0;
    struct ak_local *local = (struct ak_local *) ak_gc_alloc_object(vm->gc, sizeof(struct ak_local) + size);
    if (local != NULL) {
        local->type = AK_OBJ_LOCAL;
        local->code = code;
        local->upvals = (code->type == AK_OBJ_CODE) ? (struct ak_object **)(local + 1) : NULL;
        local->upvalnum = (code->type == AK_OBJ_CODE) ? code->upvalnum : 0;
    }
    return local;
}

static inline void ak_vm_save_cont(struct ak_vm *vm, uint32_t *pc, int noret)
{
    vm->cont->pc = pc;
    vm->cont->noret = noret;
}

static inline int ak_vm_pre_call(struct ak_vm *vm, int argnum)
{
    struct ak_cont *cont;
    struct ak_code *code;
    struct ak_local *local;

    /* vm stack: code, arg1, arg2, ..., argN */
    local = ak_vm_top(vm, -1 - argnum);
    if (local->type != AK_OBJ_LOCAL) {
        errx(-1, "vm: call not local");
    }
    code = local->code;
    if (code->type == AK_OBJ_CFUNC) {
        return ak_vm_call_cfunc(vm, argnum);
    }
    if (code->type != AK_OBJ_CODE) {
        errx(-1, "vm: function is not code object");
    }
    if (code->argnum != argnum) {
        errx(-1, "vm: expect %d arguments, but get %d arguments", code->argnum, argnum);
    }

    /* new continuation */
    vm->cont = &vm->conts[vm->funclevel++];

    /* save closure */
    cont = vm->cont;
    cont->pc = &code->opcode[0];
    cont->noret = 0;
    cont->local = local;
    /* init arguments: var index starts from 1, so local->vars[0] is unused */
    cont->vars = (struct ak_object **) &vm->stack[vm->stacktop - 1 - argnum];
    /* save position for other variables */
    memset(&vm->stack[vm->stacktop], 0x0, (code->varnum - argnum) * sizeof(void *));
    vm->stacktop += code->varnum - argnum;
    /* save top */
    cont->top = vm->stacktop;

    return -1;
}

static inline int ak_vm_post_call(struct ak_vm *vm, int retnum)
{
    int top;
    struct ak_cont *cont;

    ak_debug("vm: post call: funclevel:%d pc:%d\n", vm->funclevel, vm->cont->pc);

    /* restore original cont */
    cont = vm->cont;
    top = cont->top;

    vm->funclevel--;
    vm->cont = &vm->conts[vm->funclevel - 1];

    if (vm->stacktop != top + retnum) {
        errx(-1, "vm: func corrupted vm stack (%d != %d + %d)", vm->stacktop, top, retnum);
    }

    /* handle return value */
    /* vm stack: retval1, retval2, ..., retvaln */
    memmove(cont->vars, &vm->stack[top], retnum * sizeof(void *));
    vm->stacktop = ((void **) cont->vars - (void **) vm->stack) + retnum;

    return retnum;
}


/* AK API: */
int ak_vm_call(struct ak_vm *vm, int argnum)
{
    int retnum;

    retnum = ak_vm_pre_call(vm, argnum);
    if (retnum >= 0) {
        return retnum;
    }

    retnum = ak_vm_run(vm);     /* execute opcode */

#if (AK_VM_RUN_WITHOUT_RECURSION)
    return retnum;  /* has called ak_vm_post_call in ak_vm_run:OP RET */
#else
    return ak_vm_post_call(vm, retnum);
#endif
}

/* AK API: */
void ak_vm_set_table_cfunc(struct ak_vm *vm, struct ak_table *t, char *fname, ak_cfunc_t f)
{
    struct ak_cfunc *cf;
    struct ak_string *s;
    struct ak_local *local;
    /* cfunc */
    cf = ak_vm_cfunc(vm, f);
    if (cf == NULL) {
        errx(-1, "vm: add cfunc failed, cannot alloc cfunc");
    }
    /* local */
    local = ak_vm_local(vm, (struct ak_code *) cf);
    if (local == NULL) {
        errx(-1, "vm: cannot create local");
    }
    /* string */
    s = ak_vm_string(vm, fname, strlen(fname));
    if (s == NULL) {
        errx(-1, "vm: add cfunc failed, cannot alloc string");
    }
    /* add key,value to table */
    ak_vm_set(vm, (struct ak_object *) t, (struct ak_object *) s, (struct ak_object *) local);
}

/* AK API: */
struct ak_local *ak_vm_local_strn(struct ak_vm *vm, char *str, size_t len)
{
    struct ak_lexer *lex;
    struct ak_code *code;
    lex = ak_lex_strn(str, len);
    if (lex != NULL) {
        lex->vm = vm;
        code = ak_code_lex(lex);
        if (code->syntax_error == 0 && code != NULL) {
            ak_lex_free(lex);
            return ak_vm_local(vm, code);
        }
#if (AK_DEBUG)
        ak_code_debug(lex, code);
#endif
        ak_free(code);
        ak_lex_free(lex);
    }
    return NULL;
}

/* AK API: */
int ak_vm_call_cfunc(struct ak_vm *vm, int argnum)
{
    struct ak_local *local;
    struct ak_cfunc *cf;
    int top, retnum;
    /* enter vm stack: ?, cfunc, arg1, arg2, ... argn */
    top = vm->stacktop;
    local = ak_vm_top(vm, -1 - argnum);
    cf = (struct ak_cfunc *) local->code;
    retnum = cf->f((struct ak_vm *) vm, argnum);
#if 1   /* TODO: support multi return value */
    if (retnum != 1 && retnum != 0) {
        errx(1, "TODO: only support 0 or 1 return value currently");
    }
#endif
    if (vm->stacktop != top - 1 - argnum + retnum) {
        errx(-1, "vm: cfunc corrupted vm stack");
    }
    /* outer vm stack: ?, retval1, retval2, ... retvaln */
    return retnum;
}
