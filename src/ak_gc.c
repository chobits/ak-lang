#include <ak_gc.h>
#include <ak_alloc.h>
#include <ak_object.h>
#include <ak_debug.h>
#include <ak_list.h>
#include <ak_table.h>
#include <ak_vm.h>
#include <ak_string.h>
#include <ak_number.h>
#include <ak_code.h>

#include <err.h>

static void ak_vm_gc_scan_root(struct ak_vm *vm);

static void ak_gc_del(struct ak_gc *gc, struct ak_object *obj);
static void ak_gc_mark_gray_all(struct ak_gc *gc, struct ak_object *obj);
static void ak_gc_free_object(struct ak_gc *gc, struct ak_object *obj);


/*** gc debug ***/

static char *ak_gc_state_str[] = {
    "stop", "start", "mark", "sweep"
};

#define ak_gc_debug(gc, ...)                                        \
    do {                                                            \
        ak_debug("gc: [%s] (w:%d g:%d b:%d): ",                        \
                 ak_gc_state_str[(gc)->state],                      \
                 (gc)->whitenum, (gc)->graynum, (gc)->blacknum);    \
        ak_debug( __VA_ARGS__);                                     \
    } while (0)


/*** gc implementation ***/

/*
 * tri-color garbage collection:
 *  http://en.wikipedia.org/wiki/Tracing_garbage_collection
 */

void ak_vm_incremental_gc(struct ak_vm *vm);
static size_t ak_vm_gc_step(struct ak_vm *vm);
static size_t ak_gc_step_mark(struct ak_gc *gc);
static size_t ak_gc_step_sweep(struct ak_gc *gc);
static size_t ak_gc_step_mark_gray_all(struct ak_gc *gc, struct ak_object *obj);

/* AK API: */
void ak_vm_gc(struct ak_vm *vm)
{
    struct ak_gc *gc = vm->gc;
#ifdef AK_GC_STAT
    struct ak_gc_head gch = *(struct ak_gc_head *)gc;
    uint64_t t = ak_get_current_usec();
#endif

    if (gc->state == AK_GC_STOP) return;
    ak_vm_incremental_gc(vm);

#ifdef AK_GC_STAT
    t = ak_get_current_usec() - t;

    gc->stat.t += t;

    if (gc->stat.maxt < t) {
        gc->stat.maxt = t;
        gc->stat.maxgc = gch;
        gc->stat.maxngc = *(struct ak_gc_head *)gc;
    }

    gc->stat.count++;
#endif
}


void ak_vm_incremental_gc(struct ak_vm *vm)
{
    ssize_t gcsz;

    ak_gc_debug(vm->gc, "\n--- incremental gc\n");

    /* TODO: caculate incremental gc size */
    gcsz = AK_GC_INCREMENTAL_SIZE;

    while (gcsz > 0) {
        ak_gc_debug(vm->gc, "\n--- gc step: %zd\n", gcsz);
        gcsz -= ak_vm_gc_step(vm);
        if (vm->gc->state == AK_GC_START) { /* finish a GC cycle */
            break;
        }
    }
    ak_gc_debug(vm->gc, "\n--- incremental gc end\n");
}

static size_t ak_vm_gc_step(struct ak_vm *vm)
{
    size_t sz;
    switch (vm->gc->state) {
    case AK_GC_START:
        ak_vm_gc_scan_root(vm);
        sz = 0;
        break;
    case AK_GC_MARK:
        sz = ak_gc_step_mark(vm->gc);
        break;
    case AK_GC_SWEEP:
        sz = ak_gc_step_sweep(vm->gc);
        break;
    }
    return sz;
}

static size_t ak_gc_step_mark(struct ak_gc *gc)
{
    size_t sz;
    struct ak_object *obj;

    ak_gc_debug(gc, "mark step\n");

    sz = 0;

    /* TODO: optimize to while-mark-limit */

    if (gc->gray != NULL) {
        ak_assert(gc->graynum > 0);
        obj = gc->gray;

        ak_assert(obj->color == AK_GC_GRAY);
        obj->color = AK_GC_BLACK;
        gc->blacknum++;

        gc->gray = obj->nextgray;   /* remove from gray list */
        gc->graynum--;

#ifdef AK_ASSERT
        obj->nextgray = NULL;
#endif

        sz = ak_gc_step_mark_gray_all(gc, obj);
    }

    if (gc->gray == NULL) {
        ak_assert(gc->graynum == 0);
        ak_gc_debug(gc, "mark step end -> sweep\n\n");
        gc->state = AK_GC_SWEEP;
        gc->sweepgco = &gc->allgco;
    }

    return sz;
}

/* gray all the white objects that @obj references */
static size_t ak_gc_step_mark_gray_all(struct ak_gc *gc, struct ak_object *obj)
{
    int i;

    ak_assert(obj->type != AK_OBJ_NUM && obj->type != AK_OBJ_STR && obj->type != AK_OBJ_CFUNC);

#if (AK_DEBUG)
    ak_object_debug("gc:  +gray all: the white objects of", obj);
#endif

    if (obj->type == AK_OBJ_LIST) {
        struct ak_list *l = (struct ak_list *) obj;
        for (i = 0; i < l->num; i++) {
            ak_assert(l->array[i]->color != AK_GC_NONE);

            if (l->array[i]->color == AK_GC_WHITE) {
#if (AK_DEBUG)
                ak_object_debug("gc:   element", obj);
#endif
                ak_gc_mark_gray(gc, l->array[i]);
            }
        }
        return sizeof(struct ak_list) + sizeof(struct ak_object *) * l->maxnum;
    }

    if (obj->type == AK_OBJ_TABLE) {
        struct ak_table *t = (struct ak_table *) obj;
        for (i = 0; i < t->maxnum; i++) {
            if (t->array[i].key != NULL) {
                ak_assert(t->array[i].key->color != AK_GC_NONE);
                if (t->array[i].key->color == AK_GC_WHITE) {
#if (AK_DEBUG)
                    ak_object_debug("gc:   key", t->array[i].key);
#endif
                    ak_gc_mark_gray(gc, t->array[i].key);
                }
                if (t->array[i].value->color == AK_GC_WHITE) {
#if (AK_DEBUG)
                    ak_object_debug("gc:   value", t->array[i].value);
#endif
                    ak_gc_mark_gray(gc, t->array[i].value);
                }
            }
        }
        return sizeof(struct ak_table) + sizeof(struct ak_table_entry) * t->maxnum;
    }

    if (obj->type == AK_OBJ_CODE) {
        struct ak_code *code = (struct ak_code *) obj;
        for (i = 0; i < code->constnum; i++) {
            obj = code->constant[i];
            ak_assert(obj->color != AK_GC_NONE);

            if (obj->color == AK_GC_WHITE) {
#if (AK_DEBUG)
                ak_debug("gc:   constant[%d/%d]\n", i, code->constnum);
                ak_object_debug("gc:   constant value", obj);
#endif
                ak_gc_mark_gray(gc, obj);
            }
        }
        return sizeof(struct ak_code);
    }

    if (obj->type == AK_OBJ_LOCAL) {
        struct ak_local *local = (struct ak_local *) obj;
        if (local->code->color == AK_GC_WHITE) {
#if (AK_DEBUG)
            ak_object_debug("gc:   code", (struct ak_object *) local->code);
#endif
            ak_gc_mark_gray(gc, (struct ak_object *) local->code);
        }
        if (local->upvals == NULL) {
            return sizeof(struct ak_local);
        }
        for (i = 0; i < local->code->upvalnum; i++) {
            if ((local->upvals[i])->color == AK_GC_WHITE) {
#if (AK_DEBUG)
                ak_object_debug("gc:   upval", (struct ak_object *) local->upvals[i]);
#endif
                ak_gc_mark_gray(gc, (struct ak_object *) local->upvals[i]);
            }
        }
        return sizeof(struct ak_local) + sizeof(struct ak_object *) * local->code->upvalnum;
    }

    errx(-1, "gc: unknown type %d", obj->type);
}

static size_t ak_gc_step_sweep(struct ak_gc *gc)
{
    size_t sz;
    struct ak_object *obj;

    ak_gc_debug(gc, "sweeping\n");

    sz = 0;

    /* recycle one white objects */

    if (*gc->sweepgco) {
        struct ak_object **pobj = gc->sweepgco;
        while (1) {
            obj = *pobj;
            if (obj == NULL) {
                break;
            }
            if (obj->color == AK_GC_WHITE) {
                /*
                 * found garbage object
                 * delete it from sweep list (maybe gc->allgco)
                 */
                *pobj = obj->nextgco;
                ak_gc_free_object(gc, obj);
                sz = AK_GC_STEP_SWEEP_COST;
#if 1
                gc->whitenum--;
#endif
                break;
            }
            /*
             * flip color from BLACK to WHITE
             * does not handle their counters(gc->{white|black}num)
             */
            ak_assert(obj->color == AK_GC_BLACK);
            obj->color = AK_GC_WHITE;
#if 1
            gc->whitenum++;
            gc->blacknum--;
#endif
            pobj = &obj->nextgco;
        }
        gc->sweepgco = pobj;
    }

    if (*gc->sweepgco == NULL) {
        gc->sweepgco = NULL;
        ak_gc_debug(gc, "sweep end -> start\n\n");
        gc->state = AK_GC_START;
    }

    return sz;
}

/* AK API: */
void ak_vm_full_gc(struct ak_vm *vm)
{
    while (vm->gc->whitenum > 0 || vm->gc->graynum > 0) {
        ak_gc_debug(vm->gc, "full gc\n");
        ak_vm_gc(vm);

        if (vm->gc->state == AK_GC_START && vm->gc->graynum == 0 && vm->gc->blacknum == 0)
            break;
    }
    ak_gc_debug(vm->gc, "full gc end\n");
}

static void ak_vm_gc_scan_root(struct ak_vm *vm)
{
    int i;
    struct ak_local *local;
    struct ak_gc *gc = vm->gc;
    ak_gc_debug(gc, "scan root\n");
    /* gray objects in global */
    ak_debug("gc: gray root: global table: %p\n", vm->global);
    if (vm->global != NULL) {
        ak_gc_mark_gray(gc, (struct ak_object *) vm->global);
    }
    /* gray objects in stack */
    ak_debug("gc: gray root: stacktop[%d]\n", vm->stacktop);
    for (i = 0; i < vm->stacktop; i++) {
        ak_debug("gc: gray root: stack[%d/%d] = %p\n", i, vm->stacktop, vm->stack[i]);
        if (vm->stack[i] != NULL) {
            ak_gc_mark_gray(gc, (struct ak_object *) vm->stack[i]);
        }
    }
    /* gray objects in heap */
    ak_debug("gc: gray root: funclevel[%d]\n", vm->funclevel);
    for (i = 1; i < vm->funclevel; i++) {
        ak_debug("gc: gray root: local[%d/%d] = %p\n", i, vm->funclevel, vm->conts[i].local);
        ak_gc_mark_gray(gc, (struct ak_object *) vm->conts[i].local);
    }
    ak_gc_debug(gc, "scan root end\n\n");
    gc->state = AK_GC_MARK;
}

/* gray all the white objects that @obj references */
static void ak_gc_mark_gray_all(struct ak_gc *gc, struct ak_object *obj)
{
    int i;

    ak_assert(obj->type != AK_OBJ_NUM && obj->type != AK_OBJ_STR && obj->type != AK_OBJ_CFUNC);

#if (AK_DEBUG)
    ak_object_debug("gc: add: gray all the white objects of", obj);
#endif

    if (obj->type == AK_OBJ_LIST) {
        struct ak_list *l = (struct ak_list *) obj;
        for (i = 0; i < l->num; i++) {
            ak_assert(l->array[i]->color != AK_GC_NONE);

            if (l->array[i]->color == AK_GC_WHITE) {
#if (AK_DEBUG)
                ak_object_debug("gc:: add: gray list value", obj);
#endif
                ak_gc_mark_gray(gc, l->array[i]);
            }
        }
        return;
    }

    if (obj->type == AK_OBJ_TABLE) {
        struct ak_table *t = (struct ak_table *) obj;
        for (i = 0; i < t->maxnum; i++) {
            if (t->array[i].key != NULL) {
                ak_assert(t->array[i].key->color != AK_GC_NONE);
                if (t->array[i].key->color == AK_GC_WHITE) {
#if (AK_DEBUG)
                    ak_object_debug("gc: add:: gray table key", t->array[i].key);
#endif
                    ak_gc_mark_gray(gc, t->array[i].key);
                }
                if (t->array[i].value->color == AK_GC_WHITE) {
#if (AK_DEBUG)
                    ak_object_debug("gc: add:: gray table value", t->array[i].value);
#endif
                    ak_gc_mark_gray(gc, t->array[i].value);
                }
            }
        }
        return;
    }

    if (obj->type == AK_OBJ_CODE) {
        struct ak_code *code = (struct ak_code *) obj;
        for (i = 0; i < code->constnum; i++) {
            obj = code->constant[i];
            ak_assert(obj->color != AK_GC_NONE);

            if (obj->color == AK_GC_WHITE) {
#if (AK_DEBUG)
                ak_object_debug("gc: add:: gray code constant", obj);
#endif
                ak_gc_mark_gray(gc, obj);
            }
        }
        return;
    }

    if (obj->type == AK_OBJ_LOCAL) {
        struct ak_local *local = (struct ak_local *) obj;
        if (local->code->color == AK_GC_WHITE) {
            ak_gc_mark_gray(gc, (struct ak_object *) local->code);
        }
        if (local->upvals == NULL) {
            return;
        }
        for (i = 0; i < local->code->upvalnum; i++) {
            if ((local->upvals[i])->color == AK_GC_WHITE) {
                ak_gc_mark_gray(gc, (struct ak_object *) local->upvals[i]);
            }
        }
        return;
    }

    errx(-1, "gc: unknown type %d", obj->type);
}

/* whiten all the objects that obj references directly or undirectly */
void ak_gc_add_white_all(struct ak_gc *gc, struct ak_object *obj)
{
    int i;

    ak_assert(obj->color == AK_GC_NONE);

    ak_gc_add_white(gc, obj);

    if (obj->type == AK_OBJ_NUM || obj->type == AK_OBJ_STR || obj->type == AK_OBJ_CFUNC) {
        return;
    }

    if (obj->type == AK_OBJ_LIST) {
        struct ak_list *l = (struct ak_list *) obj;
        for (i = 0; i < l->num; i++) {
            ak_gc_add_white_all(gc, l->array[i]);
        }
        return;
    }

    if (obj->type == AK_OBJ_TABLE) {
        struct ak_table *t = (struct ak_table *) obj;
        for (i = 0; i < t->maxnum; i++) {
            if (t->array[i].key != NULL) {
                ak_gc_add_white_all(gc, t->array[i].key);
                ak_gc_add_white_all(gc, t->array[i].value);
            }
        }
        return;
    }

    if (obj->type == AK_OBJ_CODE) {
        struct ak_code *code = (struct ak_code *) obj;
        for (i = 0; i < code->constnum; i++) {
            ak_gc_add_white_all(gc, code->constant[i]);
        }
        return;
    }

    if (obj->type == AK_OBJ_LOCAL) {
        struct ak_local *local = (struct ak_local *) obj;
        ak_gc_add_white_all(gc, (struct ak_object *) local->code);
        if (local->upvals == NULL) {
            return;
        }
        for (i = 0; i < local->code->upvalnum; i++) {
            ak_gc_add_white_all(gc, (struct ak_object *) local->upvals[i]);
        }
        return;
    }
    errx(-1, "gc: unknown type %d", obj->type);
}

/* AK API: */
void ak_gc_add_white(struct ak_gc *gc, struct ak_object *obj)
{
    if (obj->color != AK_GC_NONE) {
        return;
    }
    ak_assert(obj->color != AK_GC_WHITE);
    obj->color = AK_GC_WHITE;
    gc->whitenum++;
}

/* AK API: */
void ak_gc_mark_gray(struct ak_gc *gc, struct ak_object *obj)
{
#if (AK_DEBUG)
    ak_object_debug("gc:   mark gray", obj);
#endif
    /* white -> gray or white (-> gray) -> black */
    ak_assert(obj->color != AK_GC_NONE);
    if (obj->color != AK_GC_WHITE) {
        return;
    }
    ak_assert(obj->nextgray == NULL);

    /* fastpath: mark black for objects which are not containers */
    if (obj->type == AK_OBJ_NUM || obj->type == AK_OBJ_STR || obj->type == AK_OBJ_CFUNC) {
        obj->color = AK_GC_BLACK;
        gc->whitenum--;
        gc->blacknum++;
        return;
    }

    obj->color = AK_GC_GRAY;
    gc->whitenum--;
    gc->graynum++;
    obj->nextgray = gc->gray;   /* link into gray list */
    gc->gray = obj;
}

/* AK API: */
void ak_gc_write_barrier(struct ak_gc *gc, struct ak_object *from_obj, struct ak_object *to_obj)
{
    /* ensure tri-color invariant: there is no black object referencing white objects */
    if (from_obj->color == AK_GC_BLACK && to_obj->color == AK_GC_WHITE) {
        ak_gc_mark_gray(gc, to_obj);
    }
}

#ifdef AK_GC_STAT
void ak_gc_stat_debug(struct ak_gc *gc)
{
    struct ak_gc_head *maxgc;
    extern int printf(const char *format, ...);
    printf("gc: stat: count:%u t:%u maxt:%u avgt:%.3f\n",
           gc->stat.count, gc->stat.t, gc->stat.maxt, gc->stat.t * 1.0/ gc->stat.count);

    maxgc = &gc->stat.maxgc;
    printf("gc: stat: <- max [%s] (w:%d g:%d b:%d) total:%d\n",
           ak_gc_state_str[maxgc->state], maxgc->whitenum, maxgc->graynum, maxgc->blacknum, maxgc->total);
    maxgc = &gc->stat.maxngc;
    printf("gc: stat: -> max [%s] (w:%d g:%d b:%d) total:%d\n",
           ak_gc_state_str[maxgc->state], maxgc->whitenum, maxgc->graynum, maxgc->blacknum, maxgc->total);
}
#endif

/* AK API: */
void ak_gc_free(struct ak_gc *gc)
{
#ifdef AK_GC_STAT
    ak_gc_stat_debug(gc);
#endif
    ak_free(gc);
}

/* AK API: */
struct ak_gc *ak_gc(void)
{
    struct ak_gc *gc = ak_calloc(sizeof(struct ak_gc));
    if (gc != NULL) {
        gc->state = AK_GC_STOP;
        gc->threshold = AK_GC_INIT_THRESHOLD;
        gc->total = 0;

        gc->allgco = NULL;
        gc->sweepgco = NULL;
        gc->gray = NULL;
    }
    ak_gc_debug(gc, "\n--- start gc\n");
    return gc;
}

struct ak_object *ak_gc_alloc_object(struct ak_gc *gc, size_t size)
{
    struct ak_object *obj = ak_calloc(size);
    if (obj != NULL) {
        obj->color = AK_GC_NONE;
        ak_gc_add_white(gc, obj);
        obj->nextgray = NULL;
        /* link into gc->allgco list */
        obj->nextgco = gc->allgco;
        gc->allgco = obj;
        if (gc->state == AK_GC_SWEEP && &gc->allgco == gc->sweepgco) {
            gc->sweepgco = &obj->nextgco;   /* skip new alloced object */
        }
        gc->total += size;
    }
#ifdef AK_DEBUG
    ak_object_debug("gc: alloc", obj);
#endif
    return obj;
}

void *ak_gc_alloc(struct ak_gc *gc, size_t size)
{
    void *p = ak_alloc(size);
    if (p != NULL) {
        gc->total += size;
    }
    return p;
}

void *ak_gc_realloc(struct ak_gc *gc, void *p, size_t osz, size_t sz)
{
    void *ptr = ak_realloc(p, sz);
    if (ptr != NULL) {
        gc->total += sz - osz;
    }
    return ptr;
}

static void ak_gc_free_object(struct ak_gc *gc, struct ak_object *obj)
{
#if (AK_DEBUG)
    ak_object_debug("gc: free", obj);
#endif
    /*
     * NOTE: @obj-contained object maybe freed already here.
     *
     * For example, we cannot access local->code field here.
     *  (code may be freed already by gc).
     */
    gc->total -= ak_object_size(obj);
    ak_object_free(obj);
}
