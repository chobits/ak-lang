#include <ak_vm.h>
#include <ak_gc.h>
#include <ak_alloc.h>
#include <ak_object.h>
#include <ak_number.h>
#include <limits.h>

/* return -1 for error, 0 for ok */
int akcc_strntod(char *s, size_t n, double *r)
{
    double d = 0;
    int point = 0;

    for (; n > 0; s++, n--) {
        if (*s == '.') {
            if (point != 0) {
                return -1;
            }
            point = 10;
            continue;
        }
        if (*s < '0' || *s > '9') {
            return -1;
        }
        if (point > 0 && point < INT_MAX / 10) {
            d += (double)(*s - '0') / point;
            point *= 10;
        } else if (point > 0) {
            return -1;
        } else {
            d = d * 10 + (*s - '0');
        }
    }

    if (r != NULL) {
        *r = d;
    }

    return 0;
}

/* AK API: */
int akc_number_compare(struct ak_number *a, struct ak_number *b)
{
    if (a->d < b->d) {
        return -1;
    } else if (a->d == b->d) {
        return 0;
    } else {
        return 1;
    }
}

/* AK API: */
struct ak_number *ak_vm_number(struct ak_vm *vm, double d)
{
    struct ak_number *num;

    ak_vm_gc_check(vm);

    num = (struct ak_number *) ak_gc_alloc_object(vm->gc, sizeof(struct ak_number));

    if (num != NULL) {
        num->type = AK_OBJ_NUM;
        num->d = d;
    }

    return num;
}

/* AK API: */
struct ak_number *ak_vm_number_strn(struct ak_vm *vm, char *str, size_t len)
{
    double d;
    if (akcc_strntod(str, len, &d) == -1) {
        return NULL;
    }
    return ak_vm_number(vm, d);
}

/* AK API: */
struct ak_number *ak_vm_number_add(struct ak_vm *vm, struct ak_number *a, struct ak_number *b)
{
    return ak_vm_number(vm, a->d + b->d);
}

/* AK API: */
struct ak_number *ak_vm_number_sub(struct ak_vm *vm, struct ak_number *a, struct ak_number *b)
{
    return ak_vm_number(vm, a->d - b->d);
}

/* AK API: */
struct ak_number *ak_vm_number_mul(struct ak_vm *vm, struct ak_number *a, struct ak_number *b)
{
    return ak_vm_number(vm, a->d * b->d);
}

/* AK API: */
struct ak_number *ak_vm_number_div(struct ak_vm *vm, struct ak_number *a, struct ak_number *b)
{
    return ak_vm_number(vm, a->d / b->d);
}

/* AK API: */
struct ak_number *ak_vm_number_mod(struct ak_vm *vm, struct ak_number *a, struct ak_number *b)
{
    return ak_vm_number(vm, (int) a->d % (int) b->d);
}
