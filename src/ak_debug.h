#ifndef AK_DEBUG_H
#define AK_DEBUG_H

#ifdef AK_DEBUG
#include <stdio.h>
#define ak_debug(...)                   \
    do {                                \
            (void) printf(__VA_ARGS__); \
    } while (0)
#else
#define ak_debug(...) ((void)0)
#endif

#ifdef AK_ASSERT
#include <assert.h>
#define ak_assert(...)                  \
    do {                                \
            assert(__VA_ARGS__);        \
    } while (0)
#else
#define ak_assert(...) ((void)0)
#endif

#endif  /* AK_DEBUG_H */
