#ifndef AK_STRING_H
#define AK_STRING_H

#include <stddef.h>
#include <ak_object.h>

struct ak_vm;

struct ak_string {
    AK_OBJECT_HEAD;
    size_t len; /* len-size null-terminated string is appended */
};

#define ak_str2data(s) ((unsigned char *) &((struct ak_string *) s)[1])

struct ak_string *ak_string(char *str, size_t len);
struct ak_string *ak_vm_string(struct ak_vm *vm, char *str, size_t len);
struct ak_string *ak_vm_string_concat(struct ak_vm *vm, struct ak_string *a, struct ak_string *b);
int akc_string_hash(struct ak_string *s);
int akc_string_compare(struct ak_string *a, struct ak_string *b);
int akc_string_equal(struct ak_string *a, struct ak_string *b);

#endif  /* AK_STRING_H */
