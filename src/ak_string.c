#include <ak_vm.h>
#include <ak_gc.h>
#include <ak_alloc.h>
#include <ak_object.h>
#include <ak_string.h>

#include <string.h>

int akcc_string_hash(char *s, size_t len)
{
    int hash;
    for (hash = 0; len > 0; len--, s++) {
        hash += (int) *s;   /* TODO: */
    }
    return hash;
}

int akc_string_hash(struct ak_string *s)
{
    return akcc_string_hash((char *) ak_str2data(s), s->len);
}

int akc_string_compare(struct ak_string *a, struct ak_string *b)
{
    return strcmp((const char *) ak_str2data(a), (const char *) ak_str2data(b));
}

int akc_string_equal(struct ak_string *a, struct ak_string *b)
{
    return a->len == b->len && strncmp((const char *) ak_str2data(a), (const char *) ak_str2data(b), a->len) == 0;
}

/* used by lex, TODO: delete it */
struct ak_string *ak_string(char *str, size_t len)
{
    struct ak_string *s = ak_calloc((sizeof(struct ak_string) + (len + 1) * sizeof(char)));

    if (s != NULL) {
        s->type = AK_OBJ_STR;
        memcpy(ak_str2data(s), str, len * sizeof(char));
        ak_str2data(s)[len] = '\0';
        s->len = len;
    }

    return s;
}

struct ak_string *ak_vm_string(struct ak_vm *vm, char *str, size_t len)
{
    struct ak_string *s;

    ak_vm_gc_check(vm);

    s = (struct ak_string *) ak_gc_alloc_object(vm->gc, (sizeof(struct ak_string) + (len + 1) * sizeof(char)));

    if (s != NULL) {
        s->type = AK_OBJ_STR;
        memcpy(ak_str2data(s), str, len * sizeof(char));
        ak_str2data(s)[len] = '\0';
        s->len = len;
    }

    return s;
}

struct ak_string *ak_vm_string_concat(struct ak_vm *vm, struct ak_string *a, struct ak_string *b)
{
    struct ak_string *s;

    ak_vm_gc_check(vm);

    s = (struct ak_string *) ak_gc_alloc_object(vm->gc, sizeof(struct ak_string) + (a->len + b->len + 1) * sizeof(char));

    if (s != NULL) {
        s->type = AK_OBJ_STR;
        s->len = a->len + b->len;
        memcpy(ak_str2data(s), ak_str2data(a), a->len * sizeof(char));
        memcpy(ak_str2data(s) + a->len, ak_str2data(b), b->len * sizeof(char));
        ak_str2data(s)[s->len] = '\0';
    }

    return s;
}
