#ifndef AK_LEX_H
#define AK_LEX_H

#include <ak_string.h>

struct ak_vm;

enum ak_token_type {
    AK_TK_ASSIGN = 0, /* = 0 */
    AK_TK_DOT,        /* .  */
    AK_TK_COMMA,      /* ,  */
    AK_TK_COLON,      /* :  */
    /* LOGICOP */
    AK_TK_AND,        /* and */
    AK_TK_OR,         /* or 5 */
    /* CMPOP */
    AK_TK_EQ,         /* == */
    AK_TK_NEQ,        /* != */
    AK_TK_GT,         /* >  */
    AK_TK_GE,         /* >= */
    AK_TK_LT,         /* < 10 */
    AK_TK_LE,         /* <= */
    /* BITOP */
    AK_TK_BAND,       /* & */
    AK_TK_BXOR,       /* ^ */
    AK_TK_BOR,        /* | */
    AK_TK_BSHL,       /* << 15 */
    AK_TK_BSHR,       /* >> */
    /* ADDOP */
    AK_TK_ADD,        /* +  */
    AK_TK_SUB,        /* -  */
    /* MUL */
    AK_TK_MUL,        /* *  */
    AK_TK_DIV,        /* / 20  */
    AK_TK_MOD,        /* %  */
    /* condition */
    AK_TK_IF,         /* if */
    AK_TK_ELSE,       /* else */
    AK_TK_WHILE,      /* while */
    AK_TK_BREAK,      /* break 25 */
    AK_TK_CONTINUE,   /* continue */
    AK_TK_RETURN,     /* return */
    /* funciton */
    AK_TK_FUNC,       /* func */
    /* ()[]{} */
    AK_TK_LP,         /* (  */
    AK_TK_RP,         /* ) 30 */
    AK_TK_LSP,        /* [  */
    AK_TK_RSP,        /* ]  */
    AK_TK_LAP,        /* {  */
    AK_TK_RAP,        /* }  */

    AK_TK_STR,        /* <string> 35 */
    AK_TK_VAR,        /* <variable> */
    AK_TK_NUM,        /* <number> */

    AK_TK_COMMENT,    /* comment */
    AK_TK_EOF,        /* end of file */
    AK_TK_ERR,        /* invalid token: syntax error */
    AK_TK_UNKNOWN     /* only used before parsing token */
};

#define AK_CHAR_EOF ((unsigned char) 255)
#define AK_TK_DEF_DATA_SIZE  256
#define AK_TK_MAX_DATA_SIZE  (1 << 12)  /* 4KB */

/* TODO: more generic used buffer structure */
struct ak_token_buf {
    unsigned char *start;
    unsigned char *pos;
    size_t len;
};

struct ak_token {
    int type;
    struct ak_token_buf buf;
};

struct ak_string_stream {
    unsigned char (*getchar)(struct ak_string_stream *);
    void (*ungetc)(struct ak_string_stream *, unsigned char);
    unsigned char (*nextchar)(struct ak_string_stream *);
    struct ak_string *data;
    unsigned char *pos;
};

struct ak_file_stream {
};

struct ak_lexer {
    union {
        struct ak_string_stream str;
        struct ak_file_stream file;
    } stream;
    int lineno;
    struct ak_token tk;
    unsigned int tk_lookahead:1;
    struct ak_vm *vm;
};

#define ak_lex_getchar(t) (t)->stream.str.getchar(&(t)->stream.str)
#define ak_lex_ungetc(t, c) (t)->stream.str.ungetc(&(t)->stream.str, c)
#define ak_lex_nextchar(t) (t)->stream.str.nextchar(&(t)->stream.str)

/* AK API: */
struct ak_lexer *ak_lex_strn(char *str, size_t len);
void ak_lex_free(struct ak_lexer *lex);
void ak_get_token(struct ak_lexer *lex);
void ak_unget_token(struct ak_lexer *lex);
char *ak_tk2str(int t);
void ak_lex_debug_token(struct ak_lexer *lex);

#endif  /* AK_LEX_H */
