#include <ak_vm.h>
#include <ak_gc.h>
#include <ak_alloc.h>
#include <ak_object.h>
#include <ak_list.h>
#include <ak_number.h>

#include <err.h>
#include <string.h>
#include <stdio.h>

struct ak_list *ak_vm_list(struct ak_vm *vm)
{
    struct ak_list *l;

    ak_vm_gc_check(vm);

    l = (struct ak_list *) ak_gc_alloc_object(vm->gc, sizeof(struct ak_list));

    if (l != NULL) {
        l->type = AK_OBJ_LIST;
        l->num = 0;
        l->maxnum = AK_LIST_DEFAULT_SIZE;
        l->array = ak_gc_alloc(vm->gc, sizeof(struct ak_object *) * AK_LIST_DEFAULT_SIZE); /* XXX:unnecessary to use calloc */
        if (l->array == NULL) {
            ak_free(l);
            l = NULL;
        }
    }

    return l;
}

void ak_list_free(struct ak_list *l)
{
#if (AK_DEBUG)
    memset(l->array, 0, sizeof(struct ak_object *) * l->maxnum);
#endif
    ak_free(l->array);
#if (AK_DEBUG)
    memset(l, 0, sizeof(struct ak_list));
#endif
    ak_free(l);
}

void ak_vm_list_insert(struct ak_vm *vm, struct ak_list *l, int idx, struct ak_object *obj)
{
    void *array;
    if (idx < 0 || idx > l->num) {
        errx(-1, "insert list[%d] failed", idx);
    }
    /* try to resize array */
    if (l->num == l->maxnum) {
        array = ak_gc_realloc(vm->gc, l->array, sizeof(struct ak_object *) * l->maxnum, sizeof(struct ak_object *) * l->maxnum * 2);
        if (array == NULL) {
            errx(-1, "cannot realloc l->array");
        }
        l->array = array;
        l->maxnum = l->maxnum * 2;
    }
    if (l->num != idx) {
        memmove(&l->array[idx + 1], &l->array[idx], (l->num - idx) * sizeof(struct ak_object *));
    }
    l->array[idx] = obj;
    l->num++;
    ak_gc_write_barrier(vm->gc, (struct ak_object *) l, obj);
}

void ak_vm_list_append(struct ak_vm *vm, struct ak_list *l, struct ak_object *obj)
{
    ak_vm_list_insert(vm, l, l->num, obj);
}

void ak_vm_list_set(struct ak_vm *vm, struct ak_list *l, struct ak_object *key, struct ak_object *value)
{
    int idx;
    if (key->type != AK_OBJ_NUM) {
        errx(-1, "list index is not an integer");
    }
    idx = (int) ak_num2d(key);
    if (idx >= l->num) {
        errx(-1, "list index %d is out of range %d", idx, l->num);
    }
    if (idx < 0) {
        idx = (idx % l->num) + l->num;
    }
    l->array[idx] = value;
    ak_gc_write_barrier(vm->gc, (struct ak_object *) l, value);
}

struct ak_object *ak_list_get(struct ak_list *l, struct ak_object *key)
{
    int idx;
    if (key->type != AK_OBJ_NUM) {
        errx(-1, "list index is not an integer");
    }
    idx = (int) ak_num2d(key);
    if (idx >= l->num) {
        errx(-1, "list index %d is out of range %d", idx, l->num);
    }
    if (idx < 0) {
        idx = (idx % l->num) + l->num;
    }
    return l->array[idx];
}
