#include <ak_object.h>
#include <ak_number.h>
#include <ak_string.h>
#include <ak_list.h>
#include <ak_table.h>
#include <ak_code.h>
#include <ak_alloc.h>
#include <ak_vm.h>
#include <ak_debug.h>

#include <stdio.h>
#include <string.h>


static const char *ak_type_str[] = {
    "string", "number", "code", "cfunc", "list", "table", "local"
};

static const char *ak_color_str[] = {
    "none", "white", "black", "gray"
};

/* AK API: */
char *ak_type2str(int t)
{
    return (char *) ak_type_str[t];
}

/* AK API: */
char *ak_color2str(int color)
{
    return (char *) ak_color_str[color];
}

/* AK API: */
void ak_object_debug(char *prefix, struct ak_object *obj)
{
    printf("%s (%s) <%s:%p> ", prefix, ak_color2str(obj->color), ak_type2str(obj->type), obj);

    if (obj->type == AK_OBJ_NUM) {
        printf("; %f", ((struct ak_number *) obj)->d);
    } else if (obj->type == AK_OBJ_STR) {
        printf("; \"%.*s\" %d", (int)((struct ak_string *) obj)->len, ak_str2data(obj),
                                    (int)((struct ak_string *) obj)->len);
    }

    printf("\n");
}

/* AK API: */
void ak_object_free(struct ak_object *obj)
{
    if (obj->type == AK_OBJ_LIST) {
        ak_list_free((struct ak_list *) obj);
    } else if (obj->type == AK_OBJ_TABLE) {
        ak_table_free((struct ak_table *) obj);
    } else if (obj->type == AK_OBJ_CODE) {
        ak_code_free((struct ak_code *) obj);
    } else {
#if (AK_DEBUG)
        memset(obj, 0, sizeof(struct ak_object));
#endif
        /* str/num/cfunc/local */
        ak_free(obj);
    }
}

size_t ak_object_size(struct ak_object *obj)
{
    if (obj->type == AK_OBJ_LIST) {
        struct ak_list *l = (struct ak_list *) obj;
        return sizeof(struct ak_list) + sizeof(struct ak_object *) * l->maxnum;
    } else if (obj->type == AK_OBJ_TABLE) {
        struct ak_table *t = (struct ak_table *) obj;
        return sizeof(struct ak_table) + sizeof(struct ak_table_entry) * t->maxnum;
    } else if (obj->type == AK_OBJ_CODE) {
        return sizeof(struct ak_code);
    } else if (obj->type == AK_OBJ_NUM) {
        return sizeof(struct ak_number);
    } else if (obj->type == AK_OBJ_CFUNC) {
        return sizeof(struct ak_cfunc);
    } else if (obj->type == AK_OBJ_LOCAL) {
        struct ak_local *local = (struct ak_local *) obj;
        return sizeof(struct ak_local) + (sizeof(struct ak_object *) * local->upvalnum);
    } else if (obj->type == AK_OBJ_STR) {
        struct ak_string *s = (struct ak_string *) obj;
        return sizeof(struct ak_string) + sizeof(char) * (s->len + 1);
    }
    ak_assert(0);
    return 0;
}
