#ifndef AK_LIST_H
#define AK_LIST_H

#include <ak_object.h>

struct ak_vm;

struct ak_list {
    AK_OBJECT_HEAD;
    int num;
    int maxnum;
    struct ak_object **array;
};

#define AK_LIST_DEFAULT_SIZE 16

/* AK API: */
struct ak_list *ak_vm_list(struct ak_vm *vm);
void ak_list_free(struct ak_list *l);
void ak_vm_list_insert(struct ak_vm *vm, struct ak_list *l, int idx, struct ak_object *obj);
void ak_vm_list_append(struct ak_vm *vm, struct ak_list *l, struct ak_object *obj);
struct ak_object *ak_list_get(struct ak_list *l, struct ak_object *key);
void ak_vm_list_set(struct ak_vm *vm, struct ak_list *l, struct ak_object *key, struct ak_object *value);

#endif  /* AK_LIST_H */
