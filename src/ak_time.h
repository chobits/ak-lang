#ifndef AK_TIME_H
#define AK_TIME_H

#include <stdint.h>
#include <stddef.h>
#include <sys/time.h>

extern uint64_t ak_get_current_usec(void);

#endif  /* AK_TIME_H */
