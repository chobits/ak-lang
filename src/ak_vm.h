#ifndef AK_VM_H
#define AK_VM_H

#include <stdint.h>
#include <ak_object.h>

#define AK_VM_MAX_STACK        1024
#define AK_VM_MAX_FUNCLEVEL    1024

/* runtime function context */
struct ak_local {
    AK_OBJECT_HEAD;
    struct ak_object **upvals;  /* upvalue (NULL for ak_cfunc) */
    int upvalnum;               /* upvalue number (0 for ak_cfunc) */
    struct ak_code  *code;      /* ak_code or ak_cfunc */
};

/* continuation */
struct ak_cont {
    struct ak_local *local;
    struct ak_object **vars;
    int top;
    uint32_t *pc;
    int noret;
};

struct ak_vm {
    /* int type; */
    void *stack[AK_VM_MAX_STACK];
    int stacktop;
    struct ak_cont *cont;                       /* current continuation */
    struct ak_cont conts[AK_VM_MAX_FUNCLEVEL];  /* continuations */
    int funclevel;
    struct ak_table *global;
    struct ak_gc *gc;
};

/* AK API: */
struct ak_vm *ak_vm(void);
void ak_vm_free(struct ak_vm *vm);
void ak_vm_push(struct ak_vm *vm, void *obj);
void *ak_vm_pop(struct ak_vm *vm);
void *ak_vm_top(struct ak_vm *vm, int top);
void ak_vm_set_top(struct ak_vm *vm, int top, void *obj);
int ak_vm_run(struct ak_vm *vm);
int ak_vm_call(struct ak_vm *vm, int argnum);
int ak_vm_call_cfunc(struct ak_vm *vm, int argnum);
struct ak_local *ak_vm_local(struct ak_vm *vm, struct ak_code *code);
int ak_is_false(struct ak_object *obj);

void ak_vm_set(struct ak_vm *vm, struct ak_object *t, struct ak_object *key, struct ak_object *value);
void ak_vm_set_table_cfunc(struct ak_vm *vm, struct ak_table *t, char *fname,
                           int (*f)(struct ak_vm *, int) /* ak_cfunc_t f */);
int akc_equal(struct ak_object *a, struct ak_object *b);
struct ak_local *ak_vm_local_strn(struct ak_vm *vm, char *str, size_t len);

#endif  /* AK_VM_H */
