#ifndef AK_ALLOC_H
#define AK_ALLOC_H

#include <stdlib.h>

#define ak_alloc(size) malloc(size)
#define ak_realloc(ptr, size) realloc(ptr, size)
#define ak_free(ptr) free(ptr)
#define ak_calloc(size) calloc(1, size)

#endif /* AK_ALLOC_H */
