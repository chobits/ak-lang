#include <ak_lex.h>
#include <ak_alloc.h>

#include <string.h>
#include <stdio.h>
#include <ctype.h>
#include <err.h>

static const char *ak_token_str[] = {
    "assign", "dot", "comma", "colon", "and", "or",
    "eq", "neq", "gt", "ge", "lt", "le",
    "band", "bxor", "bor", "bshl", "bshr",
    "add", "sub", "mul", "div", "mod",
    "if", "else", "while", "break", "continue", "return", "func",
    "lp", "rp", "lsp", "rsp", "lap", "rap",
    "str", "var", "num",
    "comment", "eof", "err", "unknown"
};

#define ak_char_is_number(c) isdigit(c)
#define ak_char_is_symbol(c) (isalnum(c) || ((c) == '_'))
#define ak_symbol_is(tk, str) \
    (((tk)->buf.pos - (tk)->buf.start == sizeof(str) - 1) \
     && strncmp((const char *)(tk)->buf.start, (const char *)(str), sizeof(str) - 1) == 0)

/* AK API: */
char *ak_tk2str(int t)
{
    return (char *) ak_token_str[t];
}

unsigned char ak_lex_str_getchar(struct ak_string_stream *ss)
{
    if (ss->pos < ak_str2data(ss->data) + ss->data->len) {
        return *ss->pos++;
    }
    return AK_CHAR_EOF;
}

void ak_lex_str_ungetc(struct ak_string_stream *ss, unsigned char c)
{
    if (c != AK_CHAR_EOF) {
        *--ss->pos = c;    /* NOTE: no protection */
    }
}

unsigned char ak_lex_str_nextchar(struct ak_string_stream *ss)
{
    if (ss->pos < ak_str2data(ss->data) + ss->data->len) {
        return *ss->pos;
    }
    return AK_CHAR_EOF;
}

/* AK API: */
struct ak_lexer *ak_lex_strn(char *str, size_t len)
{
    struct ak_lexer *lex;
    /* tokenizer */
    lex = ak_calloc(sizeof(struct ak_lexer));
    if (lex == NULL) {
        return NULL;
    }
    lex->lineno = 1;
    /* token */
    lex->tk.buf.start = ak_alloc(AK_TK_DEF_DATA_SIZE * sizeof(unsigned char));
    if (lex->tk.buf.start == NULL) {
        ak_free(lex);
        return NULL;
    }
    lex->tk.buf.pos = lex->tk.buf.start;
    lex->tk.buf.len = AK_TK_DEF_DATA_SIZE;
    lex->tk.type = AK_TK_UNKNOWN;
    /* string stream */
    lex->stream.str.data = ak_string(str, len); /* TODO: use ak_vm_string instead */
    if (lex->stream.str.data == NULL) {
        ak_free(lex->tk.buf.start);
        ak_free(lex);
        return NULL;
    }
    lex->stream.str.pos = ak_str2data(lex->stream.str.data);
    lex->stream.str.getchar = ak_lex_str_getchar;
    lex->stream.str.ungetc = ak_lex_str_ungetc;
    lex->stream.str.nextchar = ak_lex_str_nextchar;
    return lex;
}

void ak_lex_free(struct ak_lexer *lex)
{
    ak_free(lex->stream.str.data);  /* TODO: file stream */
    ak_free(lex->tk.buf.start);
    ak_free(lex);
}

/* save and check */
#define ak_lex_saveC(tk, c) if (ak_lex_save(tk, c) == 0) { return; }

int ak_lex_save(struct ak_token *tk, unsigned char c)
{
    unsigned char *p;
#if (AK_DEBUG)
    if (tk->buf.pos > tk->buf.start + tk->buf.len) {
        errx(-1, "ak lex: token buf pos overflows buf end");
    }
#endif
    if (tk->buf.pos == tk->buf.start + tk->buf.len) {
        if (tk->buf.len >= AK_TK_MAX_DATA_SIZE) {
            tk->type = AK_TK_ERR;
#if (AK_DEBUG)
            printf("token too long\n");
#endif
            return 0;
        }
        p = ak_realloc(tk->buf.start, tk->buf.len * 2);
        if (p == NULL) {
            tk->type = AK_TK_ERR;
            return 0;
        }
        tk->buf.start = p;
        tk->buf.pos = p + tk->buf.len;
        tk->buf.len = tk->buf.len * 2;
    }

    *tk->buf.pos++ = c;
    return 1;
}

void ak_lex_comment(struct ak_lexer *lex, struct ak_token *tk)
{
    unsigned char c;
    c = ak_lex_getchar(lex);
    if (c != '#') {
        tk->type = AK_TK_ERR;
        return;
    }

    while (1) {
        c = ak_lex_getchar(lex);
        if (c == '\r' || c == '\n' || c == AK_CHAR_EOF) {
            tk->type = AK_TK_COMMENT;
            ak_lex_ungetc(lex, c);
            return;
        }
        ak_lex_saveC(tk, c);
    }
}

void ak_lex_string(struct ak_lexer *lex, struct ak_token *tk)
{
    unsigned char c;
    int escape = 0;
    c = ak_lex_getchar(lex);
    if (c != '"') {
        tk->type = AK_TK_ERR;
        return;
    }

    while (1) {
        c = ak_lex_getchar(lex);
        if (escape == 0 && c == '"') {
            tk->type = AK_TK_STR;
            return;
        }
        if (escape == 0 && c == '\\') {
            escape = 1;
            continue;
        }
        if (c == AK_CHAR_EOF) {
            tk->type = AK_TK_ERR;
            return;
        }
        if (escape) { /* http://en.wikipedia.org/wiki/Escape_sequences_in_C */
            switch (c) {
                case 'a':
                    c = '\a';
                    break;
                case 'b':
                    c = '\b';
                    break;
                case 'f':
                    c = '\f';
                    break;
                case 'n':
                    c = '\n';
                    break;
                case 'r':
                    c = '\r';
                    break;
                case 't':
                    c = '\t';
                    break;
                case 'v':
                    c = '\v';
                    break;
                case '"':
                    c = '"';
                    break;
                case '\\':
                    c = '\\';
                    break;
                default:
                    break;
            }
            escape = 0;
        }

        ak_lex_saveC(tk, c);
    }
}

void ak_lex_number(struct ak_lexer *lex, struct ak_token *tk)
{
    unsigned char c;
    int point = 0;
#if 0
    /* make sure that at most one number char */
    c = ak_lex_nextchar(lex);
    if (!ak_char_is_number(c)) {
        tk->type = AK_TK_ERR;
        return;
    }
#endif
    while (1) {
        c = ak_lex_getchar(lex);
        if (point == 0 && c == '.') {
            point = 1;
        } else if (!ak_char_is_number(c)) {
            tk->type = AK_TK_NUM;
            ak_lex_ungetc(lex, c);
            return;
        }
        ak_lex_saveC(tk, c);
    }
}

void ak_lex_symbol(struct ak_lexer *lex, struct ak_token *tk)
{
    unsigned char c;
    /* make sure that at most one symbol char */
    c = ak_lex_nextchar(lex);
    if (!ak_char_is_symbol(c)) {
        tk->type = AK_TK_ERR;
        return;
    }
    while (1) {
        c = ak_lex_getchar(lex);
        if (!ak_char_is_symbol(c)) {
            ak_lex_ungetc(lex, c);
            break;
        }
        ak_lex_saveC(tk, c);
    }

    /* TODO: more fast parsing reserved word */
    if (ak_symbol_is(tk, "and")) {
        tk->type = AK_TK_AND;
        return;
    }
    if (ak_symbol_is(tk, "or")) {
        tk->type = AK_TK_OR;
        return;
    }
    if (ak_symbol_is(tk, "if")) {
        tk->type = AK_TK_IF;
        return;
    }
    if (ak_symbol_is(tk, "else")) {
        tk->type = AK_TK_ELSE;
        return;
    }
    if (ak_symbol_is(tk, "while")) {
        tk->type = AK_TK_WHILE;
        return;
    }
    if (ak_symbol_is(tk, "break")) {
        tk->type = AK_TK_BREAK;
        return;
    }
    if (ak_symbol_is(tk, "continue")) {
        tk->type = AK_TK_CONTINUE;
        return;
    }
    if (ak_symbol_is(tk, "return")) {
        tk->type = AK_TK_RETURN;
        return;
    }
    if (ak_symbol_is(tk, "func")) {
        tk->type = AK_TK_FUNC;
        return;
    }
    tk->type = AK_TK_VAR;
}

void ak_lex(struct ak_lexer *lex, struct ak_token *tk)
{
    int c, nc;

    tk->buf.pos = tk->buf.start;

    while (1) {
        c = ak_lex_getchar(lex);
        nc = ak_lex_nextchar(lex);

        /* end of file */
        if (c == AK_CHAR_EOF) {
            tk->type = AK_TK_EOF;
            return;
        }

        /* next */
        if (c == ' ' || c == '\t') {
            continue;
        }
        if (c == '\n' || c == '\r') {
            lex->lineno++;
            continue;
        }

        /* comment */
        if (c == '#') {
            ak_lex_ungetc(lex, c);
            ak_lex_comment(lex, tk);
            /* TODO: drop comment currently, return comment TOKEN */
            tk->buf.pos = tk->buf.start;
            continue;
        }

        /* assign */
        if (c == '=' && nc != '=') {
            tk->type = AK_TK_ASSIGN;
            return;
        }

        /* dot */
        if (c == '.') {
            tk->type = AK_TK_DOT;
            return;
        }

        /* comma */
        if (c == ',') {
            tk->type = AK_TK_COMMA;
            return;
        }

        /* colon */
        if (c == ':') {
            tk->type = AK_TK_COLON;
            return;
        }

        /* compare operator */
        if (c == '=' && nc == '=') {
            tk->type = AK_TK_EQ;
            (void) ak_lex_getchar(lex);
            return;
        }
        if (c == '!' && nc == '=') {
            tk->type = AK_TK_NEQ;
            (void) ak_lex_getchar(lex);
            return;
        }
        if (c == '>' && nc == '=') {
            tk->type = AK_TK_GE;
            (void) ak_lex_getchar(lex);
            return;
        }
        if (c == '>' && nc != '>') {
            tk->type = AK_TK_GT;
            return;
        }
        if (c == '<' && nc == '=') {
            tk->type = AK_TK_LE;
            (void) ak_lex_getchar(lex);
            return;
        }
        if (c == '<' && nc != '<') {
            tk->type = AK_TK_LT;
            return;
        }

        /* bit operator */
        if (c == '&') {
            tk->type = AK_TK_BAND;
            return;
        }
        if (c == '^') {
            tk->type = AK_TK_BXOR;
            return;
        }
        if (c == '|') {
            tk->type = AK_TK_BOR;
            return;
        }
        if (c == '<' && nc == '<') {
            tk->type = AK_TK_BSHL;
            (void) ak_lex_getchar(lex);
            return;
        }
        if (c == '>' && nc == '>') {
            tk->type = AK_TK_BSHR;
            (void) ak_lex_getchar(lex);
            return;
        }

        /* arithmetic operator */
        if (c == '+') {
            tk->type = AK_TK_ADD;
            return;
        }
        if (c == '-') {
            tk->type = AK_TK_SUB;
            return;
        }
        if (c == '*') {
            tk->type = AK_TK_MUL;
            return;
        }
        if (c == '/') {
            tk->type = AK_TK_DIV;
            return;
        }
        if (c == '%') {
            tk->type = AK_TK_MOD;
            return;
        }

        /* ()[]{} */
        if (c == '(') {
            tk->type = AK_TK_LP;
            return;
        }
        if (c == ')') {
            tk->type = AK_TK_RP;
            return;
        }
        if (c == '[') {
            tk->type = AK_TK_LSP;
            return;
        }
        if (c == ']') {
            tk->type = AK_TK_RSP;
            return;
        }
        if (c == '{') {
            tk->type = AK_TK_LAP;
            return;
        }
        if (c == '}') {
            tk->type = AK_TK_RAP;
            return;
        }

        /* <string> */
        if (c == '"') {
            ak_lex_ungetc(lex, c);
            ak_lex_string(lex, tk);
            return;
        }

        /* <number> */
        if (ak_char_is_number(c)) {
            ak_lex_ungetc(lex, c);
            ak_lex_number(lex, tk);
            return;
        }

        /* symbol: <keyword>/<variable>/<logic operator> */
        ak_lex_ungetc(lex, c);
        ak_lex_symbol(lex, tk);
        return;
    }

    /* should not be here */
}

/* AK API: */
void ak_get_token(struct ak_lexer *lex)
{
    if (lex->tk_lookahead) {
        lex->tk_lookahead = 0;
#if 0
        printf("lex: get lookahead token\n");
#endif
        return;
    }
    ak_lex(lex, &lex->tk);
#if 0
    struct ak_token *tk;
    tk = &lex->tk;
    printf("lex: line:%-8d type:%-8d(%-8s)", lex->lineno, tk->type, ak_tk2str(tk->type));
    if (tk->buf.pos != tk->buf.start) {
        printf(" -> `%.*s'\n", (int)(tk->buf.pos - tk->buf.start), tk->buf.start);
    } else {
        printf("\n");
    }
#endif
}

/* AK API: */
void ak_unget_token(struct ak_lexer *lex)
{
#if 0
    /* make sure that only one token can be looked ahead */
    ak_assert(!lex->tk_lookahead);
#endif
    lex->tk_lookahead = 1;
#if 0
    printf("lex: unget token\n");
#endif
}

void ak_lex_debug_token(struct ak_lexer *lex)
{
    struct ak_token *tk;
    while (1) {
        ak_get_token(lex);
        tk = &lex->tk;
        printf("line:%-8d type:%-8d(%-8s)", lex->lineno, tk->type, ak_tk2str(tk->type));
        if (tk->buf.pos != tk->buf.start) {
            printf(" -> `%.*s'\n", (int)(tk->buf.pos - tk->buf.start), tk->buf.start);
        } else {
            printf("\n");
        }
        if (tk->type == AK_TK_EOF) {
            break;
        }
        if (tk->type == AK_TK_ERR) {
            break;
        }
    }
}
