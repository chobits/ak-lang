#ifndef AK_TABLE_H
#define AK_TABLE_H

#include <ak_object.h>

struct ak_vm;

struct ak_table_entry {
    struct ak_object *key;
    struct ak_object *value;
};

struct ak_table {
    AK_OBJECT_HEAD;
    int num;
    int maxnum;
    struct ak_table_entry *array;
};

#define AK_TABLE_DEFAULT_SIZE 16

/* AK API: */
struct ak_table *ak_table(void);
struct ak_table *ak_vm_table(struct ak_vm *vm);
void ak_table_free(struct ak_table *t);
void ak_vm_table_set(struct ak_vm *vm, struct ak_table *t, struct ak_object *key, struct ak_object *value);
void ak_table_set(struct ak_table *t, struct ak_object *key, struct ak_object *value);
struct ak_object *ak_table_get(struct ak_table *t, struct ak_object *key);
struct ak_object *ak_table_get_kv(struct ak_table *t, struct ak_object *key, struct ak_object **okey);
void ak_table_delete(struct ak_table *t, struct ak_object *key);
void ak_table_free_string_keys(struct ak_table *t);

#endif  /* AK_TABLE_H */
